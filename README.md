
# Chordata Software

The repository contains a software package for the open-source Motion Capture System created by Chordata. Furthermore, there are project examples to be used with the basic software "Mover", and utilities for a Beta Version of the system.

All of it has been developed with [openFrameworks](https://openframeworks.cc/).

----

## Software

* **README.md** >> Introduction and guideline for Chordata Sizer, Chordata Mover, and Examples
* **files** >> Project files with source code, data, addons and executable (Linux 64bit)
* **images** >> Photos and screenshots of Software &
* **videos** >> Photos and screenshots of Software &

### Chordata Sizer

Chordata Sizer is a editor for a calibration file, that creates a performer file, that will be used in Chordata Mover. The file will create an accurate representing skeleton.

![Screenshot](software/images/Chordata_Sizer_screenshot.jpg)

### Chordata Mover

Chordata Mover receives quaternion data of gyroscope sensors from Notochord Software via OSC on port 6565. This data are linked to a representing skeleton.

![Screenshot](software/images/Chordata_Mover_screenshot.jpg)


### Examples
This examples are project examples, that can be programmed based on this code. It has all in it to start up with your own projects.

#### Example: Particulus

![Screenshot](software/images/Example_Particulus_screenshot.jpg)

#### Example: Mime Corporeal

![Screenshot](software/images/Example_MimeCorporeal_screenshot.jpg)

---

## Utilities

The repository contains utilities for the Chordata project, including designs for hardware and software supporting equipment.


* **README.md** >> Photo assembly guide

* **shutdown_button** >> Python script and service script for  Raspberry Pi Shutdown Button connected to GPIO Pin 20

* **relativity_cube >>**

  * **img** >> Renders and photo of Design
  * **scad** >> Source file in .scad format
  * **stl** >> Exported 3D meshes in .stl format

  | Render | Photo |
  | ----------- | ----------- |
  | ![](./utilities/relativity_cube/images/relativity_cube_render.jpg)| ![](./utilities/relativity_cube/images/relativity_cube_final.jpg) |

* **kceptor_case >>**

  * **gcode** >> Example files of working gcode printable files >>
  * **img** >> Renders and photo of Design
  * **scad** >> Source file in .scad format
  * **stl** >> Exported 3D meshes in .stl format

  | Render | Photo |
  | ----------- | ----------- |
  | ![](./utilities/kceptor_case/images/kceptor_case.png)| ![](./utilities/kceptor_case/images/kceptor_case_Photo.png) |



* **rpi_hub_case >>**

  * **gcode** >> Example files of working gcode printable files
  * **img** >> Renders and photo of Design
  * **scad** >> Source file in .scad format
  * **stl** >> Exported 3D meshes in .stl format

  | Render | Photo |
  | ----------- | ----------- |
  | ![](./utilities/rpi_hub_case/images/rpi_hub_case.png)| ![](./utilities/rpi_hub_case/images/rpi_hub_case_Photo.png) |
