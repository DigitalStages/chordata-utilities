#include "ofApp.h"
#include <iostream>
#include <sstream>
using namespace std;
using namespace pal::ofxSkeleton;

//--------------------------------------------------------------
void ofApp::setup(){

    // GENERAL SETTINGS ================================================
    ofSetFrameRate(60);
    ofSetVerticalSync(false);
    ofEnableSmoothing();
    portIn = 6565;
    portOut = 6568;

    width = ofGetWidth();
    height = ofGetHeight();
    bGyros = false;
    bThreeD = true;
    bLive = false;
    startCount = true;
    fileSettings = "settings.xml";
    fileSizer = "default.xml";

    // GYROS ================================================
    gyrosCount = 15;
    kc_quaternion.set(0, 0, 0, 1);
    for(int i = 0; i < gyrosCount; i++)
    {
        kc_quats.push_back(kc_quaternion);
        quatNodes.push_back(gRot);
        zeroG.push_back(kc_quaternion);
        zeroS.push_back(kc_quaternion);
        zeroDiffG.push_back(kc_quaternion);
        zeroDiffS.push_back(kc_quaternion);
    }
    for(int i = 0; i < gyrosCount; i++)
    {
        quatNodes[i].setOrientation(kc_quaternion);
    }

    // GYROS VIEW ================================================
    ofSetCircleResolution(64);
    gyrosCam.setPosition(0,0,380);
    gyrosCam.enableOrtho();
    gyrosCam.removeAllInteractions();
    gyrosCam.disableMouseMiddleButton();
    gyrosCam.setNearClip(-2000000);
    gyrosCam.setFarClip(2000000);
    // Add needed mouse navigation actions in combination with shortkeys
    gyrosCam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_LEFT, OF_KEY_SHIFT);
    gyrosCam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_LEFT);
    gyrosCam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_RIGHT, OF_KEY_SHIFT);
    gyrosCam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_RIGHT);
    // Add Gyro Labels to vector array
    gLbls.push_back("CH_6_0");
    gLbls.push_back("CH_6_1");
    gLbls.push_back("CH_6_2");
    gLbls.push_back("CH_5_0");
    gLbls.push_back("CH_4_0");
    gLbls.push_back("CH_4_1");
    gLbls.push_back("CH_4_2");
    gLbls.push_back("CH_3_0");
    gLbls.push_back("CH_3_1");
    gLbls.push_back("CH_3_2");
    gLbls.push_back("CH_2_1");
    gLbls.push_back("CH_2_2");
    gLbls.push_back("CH_1_0");
    gLbls.push_back("CH_1_1");
    gLbls.push_back("CH_1_2");
    //GYRO VIEW GRID
    gapSize = 10;
    lineCount = 44;
    gridColor.set(80);
    lblColor.set(240, 240, 240);
    colorSelect.set(255, 165, 0);
    curJoint = 0;
    dist = 100;
    // 3D VIEWPORT ===================================================
    // Set camera
    threeDCam.setPosition(1200, 600, 1200);
    threeDCam.lookAt(ofVec3f(0));
    // Modify mouse interactivity
    threeDCam.removeAllInteractions();
    threeDCam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_LEFT, OF_KEY_SHIFT);
    threeDCam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_RIGHT, OF_KEY_SHIFT);
    threeDCam.addInteraction(ofEasyCam::TRANSFORM_ROTATE, OF_MOUSE_BUTTON_LEFT);
    threeDCam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_RIGHT);
    // Set reference distance of cube in millimeters
    refDist = 500;
    // Initiate default position values of joints
    Hips.set(0, 0, 0);
    Spine.set(0, 29, 2);
    Chest.set(0, 75, 1);
    Neck.set(0, 151, 0);
    Head.set(0, 192, 14);
    HeadEnd.set(0, 255, 6);
    LeftUpLeg.set(50, -27, 12);
    LeftLeg.set(42, -180, 7);
    LeftFoot.set(43, -329, -3);
    LeftFootEnd.set(45, -360, 38);
    RightUpLeg.set(-46, -27, 12);
    RightLeg.set(-38, -180, 7);
    RightFoot.set(-39, -329, -3);
    RightFootEnd.set(-41, -360, 38);
    LeftShoulder.set(12, 141, 26);
    LeftArm.set(60, 138, 14);
    LeftForeArm.set(75, 44, -4);
    LeftHand.set(89, -55, 18);
    LeftHandEnd.set(92, -88, 22);
    RightShoulder.set(-8, 141, 26);
    RightArm.set(-56, 138, 14);
    RightForeArm.set(-71, 44, -4);
    RightHand.set(-85, -55, 18);
    RightHandEnd.set(-88, -88, 22);
    offset.set(0, 360, 0);
    floorDist.set(0, 0);
    fixPoint.set(0, 0, 0);
    fixPointNew.set(0, 0, 0);
    // Add reference points of joints to vector array
    joints.push_back(Hips);
    joints.push_back(Spine);
    joints.push_back(Chest);
    joints.push_back(Neck);
    joints.push_back(Head);
    joints.push_back(HeadEnd);
    joints.push_back(LeftUpLeg);
    joints.push_back(LeftLeg);
    joints.push_back(LeftFoot);
    joints.push_back(LeftFootEnd);
    joints.push_back(RightUpLeg);
    joints.push_back(RightLeg);
    joints.push_back(RightFoot);
    joints.push_back(RightFootEnd);
    joints.push_back(LeftShoulder);
    joints.push_back(LeftArm);
    joints.push_back(LeftForeArm);
    joints.push_back(LeftHand);
    joints.push_back(LeftHandEnd);
    joints.push_back(RightShoulder);
    joints.push_back(RightArm);
    joints.push_back(RightForeArm);
    joints.push_back(RightHand);
    joints.push_back(RightHandEnd);
    for(int i = 0; i < int(joints.size()); i++)
    {
        joints[i].y = joints[i].y + offset.y;
    }
    // Add joint labels to vector array
    jLbls.push_back("Hips");            //0 >> CH_5_0
    jLbls.push_back("Spine");           //1
    jLbls.push_back("Chest");           //2 >> CH_2_1
    jLbls.push_back("Neck");            //3
    jLbls.push_back("Head");            //4 >> CH_2_2
    jLbls.push_back("HeadEnd");         //5
    jLbls.push_back("LeftUpLeg");       //6 >> CH_4_0
    jLbls.push_back("LeftLeg");         //7 >> CH_4_1
    jLbls.push_back("LeftFoot");        //8 >> CH_4_2
    jLbls.push_back("LeftFootEnd");     //9
    jLbls.push_back("RightUpLeg");      //10 >> CH_6_0
    jLbls.push_back("RightLeg");        //11 >> CH_6_1
    jLbls.push_back("RightFoot");       //12 >> CH_6_2
    jLbls.push_back("RightFootEnd");    //13
    jLbls.push_back("LeftShoulder");    //14
    jLbls.push_back("LeftArm");         //15 >> CH_1_0
    jLbls.push_back("LeftForeArm");     //16 >> CH_1_1
    jLbls.push_back("LeftHand");        //17 >> CH_1_2
    jLbls.push_back("LeftHandEnd");     //18
    jLbls.push_back("RightShoulder");   //19
    jLbls.push_back("RightArm");        //20 >> CH_3_0
    jLbls.push_back("RightForeArm");    //21 >> CH_3_1
    jLbls.push_back("RightHand");       //22 >> CH_3_2
    jLbls.push_back("RightHandEnd");    //23
    gjLbls.push_back(jLbls[12]);
    gjLbls.push_back(jLbls[11]);
    gjLbls.push_back(jLbls[10]);
    gjLbls.push_back(jLbls[0]);
    gjLbls.push_back(jLbls[8]);
    gjLbls.push_back(jLbls[7]);
    gjLbls.push_back(jLbls[6]);
    gjLbls.push_back(jLbls[22]);
    gjLbls.push_back(jLbls[21]);
    gjLbls.push_back(jLbls[20]);
    gjLbls.push_back(jLbls[4]);
    gjLbls.push_back(jLbls[2]);
    gjLbls.push_back(jLbls[17]);
    gjLbls.push_back(jLbls[16]);
    gjLbls.push_back(jLbls[15]);
    //Set up skeleton visualization
    for(int i = 0; i < int(joints.size()); i++)
    {
        Skeleton[jLbls[i]] = JointP_t(new ofxJoint());
        Skeleton[jLbls[i]]->setPosition(joints[i]);
    }
    Skeleton[jLbls[1]]->bone(Skeleton[jLbls[0]]);
    Skeleton[jLbls[2]]->bone(Skeleton[jLbls[1]]);
    Skeleton[jLbls[3]]->bone(Skeleton[jLbls[2]]);
    Skeleton[jLbls[4]]->bone(Skeleton[jLbls[3]]);
    Skeleton[jLbls[5]]->bone(Skeleton[jLbls[4]]);
    Skeleton[jLbls[6]]->bone(Skeleton[jLbls[0]]);
    Skeleton[jLbls[7]]->bone(Skeleton[jLbls[6]]);
    Skeleton[jLbls[8]]->bone(Skeleton[jLbls[7]]);
    Skeleton[jLbls[9]]->bone(Skeleton[jLbls[8]]);
    Skeleton[jLbls[10]]->bone(Skeleton[jLbls[0]]);
    Skeleton[jLbls[11]]->bone(Skeleton[jLbls[10]]);
    Skeleton[jLbls[12]]->bone(Skeleton[jLbls[11]]);
    Skeleton[jLbls[13]]->bone(Skeleton[jLbls[12]]);
    Skeleton[jLbls[14]]->bone(Skeleton[jLbls[2]]);
    Skeleton[jLbls[15]]->bone(Skeleton[jLbls[14]]);
    Skeleton[jLbls[16]]->bone(Skeleton[jLbls[15]]);
    Skeleton[jLbls[17]]->bone(Skeleton[jLbls[16]]);
    Skeleton[jLbls[18]]->bone(Skeleton[jLbls[17]]);
    Skeleton[jLbls[19]]->bone(Skeleton[jLbls[2]]);
    Skeleton[jLbls[20]]->bone(Skeleton[jLbls[19]]);
    Skeleton[jLbls[21]]->bone(Skeleton[jLbls[20]]);
    Skeleton[jLbls[22]]->bone(Skeleton[jLbls[21]]);
    Skeleton[jLbls[23]]->bone(Skeleton[jLbls[22]]);
    // set joint names according to their map indices.
    for (map<string, JointP_t>::iterator it = Skeleton.begin(); it != Skeleton.end(); ++it){
        it->second->setName(it->first);
    }
    // Get orientation from the skeleton, which has been build via position
    for(int i = 0; i < gyrosCount; i++){
        if(i == 0){
            zeroS[i] = Skeleton[jLbls[12]]->getOrientation();
        }
        else if(i == 1){
            zeroS[i] = Skeleton[jLbls[11]]->getOrientation();
        }
        else if(i == 2){
            zeroS[i] = Skeleton[jLbls[10]]->getOrientation();
        }
        else if(i == 3){
            zeroS[i] = Skeleton[jLbls[0]]->getOrientation();
        }
        else if(i == 4){
            zeroS[i] = Skeleton[jLbls[8]]->getOrientation();
        }
        else if(i == 5){
            zeroS[i] = Skeleton[jLbls[7]]->getOrientation();
        }
        else if(i == 6){
            zeroS[i] = Skeleton[jLbls[6]]->getOrientation();
        }
        else if(i == 7){
            zeroS[i] = Skeleton[jLbls[22]]->getOrientation();
        }
        else if(i == 8){
            zeroS[i] = Skeleton[jLbls[21]]->getOrientation();
        }
        else if(i == 9){
            zeroS[i] = Skeleton[jLbls[20]]->getOrientation();
        }
        else if(i == 10){
            zeroS[i] = Skeleton[jLbls[4]]->getOrientation();
        }
        else if(i == 11){
            zeroS[i] = Skeleton[jLbls[2]]->getOrientation();
        }
        else if(i == 12){
            zeroS[i] = Skeleton[jLbls[17]]->getOrientation();
        }
        else if(i == 13){
            zeroS[i] = Skeleton[jLbls[16]]->getOrientation();
        }
        else if(i == 14){
            zeroS[i] = Skeleton[jLbls[15]]->getOrientation();
        }
        else{
            cout << "More Gyros are available" << endl;
        }
    }
    //Set quaternion for joints, that do not have gyro data
    offsetRot.set(0, 0, 0, 1);
    // TEXT PANELS ===================================
    // Text
    zekton.load("zekton.ttf", 8, false);
    zektonGyro.load("zekton.ttf", 16, true);
    zektonGyro.setGlobalDpi(96);
    zektonGyro.setLineHeight(18.0f);
    zektonGyro.setLetterSpacing(1.037);
    zektonCount.load("zekton.ttf", 32, true);
    zektonCount.setGlobalDpi(96);
    // Booleans
    bHelp = false;
    bGuide = false;
    bAbout = false;
    // GUI PANELS =====================================
    guiWidth = 100;
    guiHeight = 20;
    bGuiG = true;
    bGui3D = true;
    tglFull = false;
    gData = false;
    recording = false;
    playing = false;
    tglTrans = false;
    sending = false;
    fContact = false;
    setFixPoint = true;
    fixedJoint = 13;
    strOffsetTD = "10";
    fThres= 10;
    h2nVal = 0;
    c2sVal = 0;
    la2lsVal = 0;
    ra2rsVal = 0;
    pIn = ofToString(portIn);
    host = "localhost";
    portOut = 6568;
    pOut = ofToString(portOut);
    receiver.setup(portOut);
    gC = ofColor(0, 165, 220);
    tC = ofColor(125, 155, 0);
    pC = ofColor(180, 40, 120);
    oC = ofColor(163, 180, 0);
    fixedJC = ofColor(255, 255, 0);
    //Load settings if file exists
    ofFile settings;
    settings.open(fileSettings);
    if(settings.isFile())
    {
        loadDefault();
    }
    else{
        cout<<"Default settings file does not exist."<<endl;
    }
    hipsPos.set(Hips + offset);
    offsetTD = ofToInt(strOffsetTD);
    // GUI of Gyro View
    guiG.setup("Gyro View Menu");
    guiG.add(prefG.setup("GENERAL SETTINGS", "======"));
    guiG.add(tglFullG.setup("Toggle Fullscreen"));
    prefG.setBackgroundColor(gC);
    tglFullG.addListener(this, &ofApp::fullscreenButtonPressed);
    guiG.add(viewBtn3D.setup("3D View"));
    viewBtn3D.addListener(this, &ofApp::viewButtonPressed);
    guiG.add(zeroBtnG.setup("Offset Gyroscopes"));
    zeroBtnG.addListener(this, &ofApp::offsetGyros);
    guiG.add(gyroData.setup("Show Gyro Data", gData));


    // GUI of Skeleton View
    gui3D.setup("3D VIEW MENU");
    gui3D.add(generalGroup.setup("GENERAL SETTINGS ======"));
    generalGroup.setHeaderBackgroundColor(gC);
    generalGroup.add(portInInput.set("Receiver Port: ", pIn));
    portInInput.addListener(this, &ofApp::portInChanged);
    generalGroup.add(tglFull3D.setup("Toggle Fullscreen"));
    tglFull3D.addListener(this, &ofApp::fullscreenButtonPressed);
    generalGroup.add(viewBtnG.setup("Gyros View"));
    viewBtnG.addListener(this, &ofApp::viewButtonPressed);
    gui3D.add(perfGroup.setup("PERFORMER SETTINGS ===="));
    perfGroup.setHeaderBackgroundColor(pC);
    perfGroup.add(importSettings.setup("Import Settings"));
    importSettings.addListener(this, &ofApp::importSettingsFile);
    perfGroup.add(exportSettings.setup("Export Settings"));
    exportSettings.addListener(this, &ofApp::exportSettingsFile);
    perfGroup.add(importSize.setup("Import Sizer File"));
    importSize.addListener(this, &ofApp::importPerformerFile);
    perfGroup.add(goLive.setup("GO LIVE!", bLive));
    perfGroup.add(offsetDelay.set("Offset Timedelay: ", strOffsetTD));
    offsetDelay.addListener(this, &ofApp::delayOffset);
    perfGroup.add(zeroBtn3D.setup("Offset Gyroscopes"));
    zeroBtn3D.addListener(this, &ofApp::offsetCountDown);
    perfGroup.add(floorFix.setup("Floor Contact", fContact));
    perfGroup.add(floorThres.setup("Floor Threshold", 0, -fThres, fThres));
//    gui3D.add(head2Neck.setup("Head >> Neck", h2nVal, 0, 100));
//    gui3D.add(chest2Spine.setup("Chest >> Spine", c2sVal, 0, 100));
//    gui3D.add(lArm2lShoulder.setup("LArm >> LShoulder", la2lsVal, 0, 100));
//    gui3D.add(rArm2rShoulder.setup("RArm >> RShoulder", ra2rsVal, 0, 100));
//    gui3D.add(recLbl.setup("RECORD SETTINGS", "======="));
//    gui3D.add(newRec.setup("New Motion"));
//    gui3D.add(record.setup("Start/Stop Recording", recording));
//    recording delay
//    gui3D.add(play.setup("Start/Stop Playing", playing));
//    gui3D.add(importRec.setup("Import Motion"));
//    gui3D.add(exportRec.setup("Export Motion"));
    gui3D.add(oscGroup.setup("OSC SETTINGS =========="));
    oscGroup.setHeaderBackgroundColor(oC);
    oscGroup.add(sendOsc.setup("Send Data via OSC", sending));
    oscGroup.add(sendTrans.setup("Send Translation", tglTrans));
    oscGroup.add(hostInput.set("Host: ", host));
    hostInput.addListener(this, &ofApp::hostChanged);
    oscGroup.add(portOutInput.set("Port: ", pOut));
    portOutInput.addListener(this, &ofApp::portOutChanged);


    // Timeline of Skeleton View
    tlHeight = height / 9;
    parameters.setName("Performer Data");
//    parameters.add(hips);
    parameters.add(color.set("color", 100, ofColor(0,0),255));
//    for(int i = 0; i > int(joints.size()); i++)
//    {
//        parameters.add()
//    }

    tlGui.setup(parameters, "", 0, 0);
    /// then simply add your ofxPanel to the timeline.
    timeline.add(tlGui);
    /// The timeline also works without its view, so you need to generate it.
    /// This can be done and undone on runtime, in order to show or hide the timeline.
    /// look further down in the keyReleased function to see how to create or destroy the view.
    timeline.generateView();
    /// By default ofxLineaDeTiempo will auto adjust itself to fill the whole window in which it is being drawn.
    /// if you uncomment the following line, the timeline's shape will be set and the autofill property will get disabled.
    /// When you do so, the ofxLineaDeTiempo instance will now be drawn with a header bar, which you can clic and drag to drag the whole timeline around. Aswell it will have a handle in its lowerright corner which you can use for resizing the timeline.
    /// Now lets make the timeline to use half of the window so we leave some space for the other graphics to be drawn.
    timeline.setShape(ofRectangle(0, height - tlHeight, width, height));
    ///\ Lets load some data into the timeline
    ///\ You can save this data pressing the 's' key.
    //timeline.load( ofToDataPath("timelineData.json"));
    // Toggle timeline to fullscreen
    tlFullscreen = false;

    portIn = ofToInt(pIn);
    ofLog() << "listening for osc messages on port " << portIn;
    receiver.setup(portIn);

    // Settung Osc out
    //threadedOsc.setup(int(joints.size()), host, portOut);
    sender.setup(host, portOut);
}

//--------------------------------------------------------------
void ofApp::update(){
    // Get width and height of window
    width = ofGetWidth();
    height = ofGetHeight();
    if(goLive)
    {
        // Listen to messages and add those received to vector
        oscMessage();
        if(sendOsc)
        {
            if(sendTrans)
            {
                sendingTranslation();
            }
            else{ sendingRotation();}
        }
    }
    //Send data via OSC as soon sending is activated
    if(bThreeD == true)
    {
        //Fix joints that hit the floor
        if(floorFix == true)
        {
            for(int i = 0; i < int(joints.size()); i++)
            {
                // Recognize joint that is below the set threshold line
                if(Skeleton[jLbls[i]]->getGlobalPosition().y <= floorThres)
                {
                    if(i != fixedJoint && Skeleton[jLbls[i]]->getGlobalPosition().y <= Skeleton[jLbls[fixedJoint]]->getGlobalPosition().y)
                    {
                        fixedJoint = i;
                        floorDist.x = floorDist.x - fixPoint.x;
                        floorDist.y = floorDist.y - fixPoint.z;
                        hipsPos.x = hipsPos.x + floorDist.x;
                        hipsPos.z = hipsPos.z + floorDist.y;
                        setFixPoint = true;
                        cout<<fixedJoint<<endl;
                    }
                }
            }
            // Move the hip
            if(setFixPoint == true)
            {
                //Get current position of fixed joint
                floorDist.x = floorDist.x + Skeleton[jLbls[fixedJoint]]->getGlobalPosition().x;
                floorDist.y = floorDist.y + Skeleton[jLbls[fixedJoint]]->getGlobalPosition().z;
                fixPoint.set(floorDist.x, Skeleton[jLbls[fixedJoint]]->getGlobalPosition().y, floorDist.y);
                cout<<"FixPoint: " + ofToString(fixPoint)<<endl;
                setFixPoint = false;
            }
            floorContact(fixedJoint);
            cout<<"FloorDist: " + ofToString(floorDist)<<endl;
        }
        else if(floorFix == false){
            floorDist.set(0, 0);
            fixPoint.set(0, 0, 0);
            hipsPos.set(Hips + offset);
            Skeleton[jLbls[0]]->setGlobalPosition(hipsPos);
            setFixPoint = false;

        }
    }
    if(tglFull == true) { ofSetFullscreen(true); }
    else if(tglFull == false) { ofSetFullscreen(false); }
}

//--------------------------------------------------------------
void ofApp::draw(){
    //Activate Gyros View
    if(bGyros == true)
    {
        ofSetBackgroundColor(120);
        //Switch off 3D viewport
        // GYRO VIEW ===================================================================
        gyrosCam.begin();
        // Display grid
        grid(gapSize, lineCount);
        // Draw Labels
        gridsize = dist * 4;
        ofSetColor(255);
        //Set rotation of each gyro, position and draw it in Gyro View
        for(int i = 0; i < gyrosCount; i++)
        {
            for(int j = 0; j < 4; j++){
                ofPushMatrix();
                gyronumber = j;
                ofTranslate(-gridsize / 2.7 + (dist * gyronumber), gridsize - dist * 2.5);
                quatNodes[j].transformGL();
                gyro(dist / 3);
                quatNodes[j].restoreTransformGL();
                if(gyroData && j == curJoint) {ofSetColor(colorSelect);}
                else{ofSetColor(lblColor);}
                zekton.drawString(gLbls[j], -dist / 4, -dist / 2.5);
                ofPopMatrix();
            }
            for(int k = 4; k >= 4 && k < 8; k++){
                ofPushMatrix();
                gyronumber = k;
                ofTranslate(-gridsize / 2.7 + (dist * (gyronumber - 4)), dist * 0.5);
                quatNodes[k].transformGL();
                gyro(dist / 3);
                quatNodes[k].restoreTransformGL();
                if(gyroData && k == curJoint) {ofSetColor(colorSelect);}
                else{ofSetColor(lblColor);}
                zekton.drawString(gLbls[k + 4], -dist / 4, -dist / 2.5);
                ofPopMatrix();
            }
            for(int l = 8; l >= 8 && l < 12; l++){
                ofPushMatrix();
                gyronumber = l;
                ofTranslate(-gridsize / 2.7 + (dist * (gyronumber - 8)), -dist / 2);
                quatNodes[l].transformGL();
                gyro(dist / 3);
                quatNodes[l].restoreTransformGL();
                if(gyroData && l == curJoint) {ofSetColor(colorSelect);}
                else{ofSetColor(lblColor);}
                zekton.drawString(gLbls[l], -dist / 4, -dist / 2.5);
                ofPopMatrix();
            }
            for(int m = 12; m >= 12 && m < 15; m++){
                ofPushMatrix();
                gyronumber = m;
                ofTranslate(-gridsize / 2.7 + (dist * (gyronumber - 12)), -(dist * 1.5));
                quatNodes[m].transformGL();
                gyro(dist / 3);
                quatNodes[m].restoreTransformGL();
                if(gyroData && m == curJoint) {ofSetColor(colorSelect);}
                else{ofSetColor(lblColor);}
                zekton.drawString(gLbls[m], -dist / 4, -dist / 2.5);
                ofPopMatrix();
            }
        }
        //end gyro view section
        gyrosCam.end();
        //If GUI is toggled, draw the gui panel
        if(bGuiG == true){
            guiG.draw();
        }
        if(gyroData)
        {
            ofSetColor(colorSelect);
            zektonGyro.drawString(gjLbls[curJoint] + ": " + ofToString(quatNodes[curJoint].getOrientationQuat()), 30, height - 30);
            ofSetColor(255);
        }
    }
    //Activate 3D View port
    if(bThreeD == true)
    {
        rotateSkeleton();
        threeDCam.begin();
        //Set background color
        ofSetBackgroundColor(30);
        // Display grid
        ofSetLineWidth(1);
        ofDrawGrid(dist, 16, false, false, true, false);
        ofPushMatrix();
        //Draw skeleton
        for (map<string, JointP_t>::iterator it = Skeleton.begin(); it != Skeleton.end(); ++it){
            it->second->draw(10);
        }
        ofPopMatrix();

        //End 3D view section
        threeDCam.end();
        if(bGui3D == true){
            gui3D.draw();
            timeline.draw();
        }
    }
    //Activate/Deactivate fullscreen of timeline
    if (tlFullscreen == true){
        timeline.setAutoFillScreen(true);
        timeline.setShape(ofRectangle(0, 0, width, height));
    }
    else if(tlFullscreen == false) {
        timeline.setAutoFillScreen(false);
        timeline.setShape(ofRectangle(0, height - tlHeight, width, tlHeight));
    }
    if(counting == true)
    {
        offsetCountDown();
        ofSetColor(30);
        ofDrawCircle(width / 2, height / 2, 30);
        ofSetColor(255, 255, 0);
        zektonCount.drawString(ofToString(countDown), width / 2 - 12, height / 2 + 16);
        ofSetColor(255);
    }

    //PANELS ====================================================================
    // If "h" is pressed, help text will appear
    if (bHelp == true) {
        bGuide = false;
        bAbout = false;
        ofSetColor(50, 50, 50, 255);
        ofDrawRectangle(width * 0.05, height * 0.05, width * 0.9, height * 0.9);
        ofSetColor(0, 255, 0);
        stringstream ss;
        ss << "Chordata Mover >> Help ======================================================================================"<<endl;
        ss << endl;
        ss << "KEY MAP" << endl;
        ss << endl;
        ss << "'h' >> Shows this panel              'g' >> Shows a 'How to' guide           'a' >> Shows software info" << endl;
        ss << "'t' >> Maximize timeline             'f' >> Toggle fullscreen                'm' >> Toggle GUI Menu/Timeline" << endl;
        ss << "'1' >> Gyro View                     '2' >> 3D View Port                     'i' >> Imports a performer file" << endl;
        ss << "" << endl;
        ss << endl;
        ss << endl;
        ss << "GYRO VIEW ==================================================================================================="<<endl;
        ss << endl;
        ss << "LMB or RMB + SHIFT >> Pan view             RMB or LMB + SHIFT >> Zoom view" << endl;
        ss << endl;
        ss << "GUI" << endl;
        ss << endl;
        ss << "'Offset Gyroscopes' >> Offset Gyros arrow to zero position" << endl;
        ss << "'Show Gyro Data' >> Shows each Gyros Quaternion data (Use Up and Down Arrow Keys)" << endl;
        ss << "'Toggle Fullscreen' >> Guess what!" << endl;
        ss << "'3D View' >> Switch over to 3D View Port" << endl;
        ss << endl;
        ss << endl;
        ss << "3D VIEW ====================================================================================================="<<endl;
        ss << endl;
        ss << "LMB >> Rotate view                         SHIFT + LMB >> Pan view" << endl;
        ss << "RMB >> Pan view                            SHIFT + RMB >> Zoom view" << endl;
        ss << endl;
        ss << "GUI" << endl;
        ss << endl;
        ss << "'Import/Export Settings' >> Imports / Exports Settings set in this Gui Panel" << endl;
        ss << "'Import Sizer File' >> Imports Performer File made in Chordata Sizer" << endl;
        ss << "'Offset Gyroscopes' >> Offset Gyros arrow to zero position " << endl;
        ss << "'Offset Timedelay' >> Sets a countdown in seconds (Default 5) >> Set 0 to disable" << endl;
        ss << "'Floor Contact' >> Toggles floor recognition | 'Floor Threshold' tunes IRL Data <<>> VR representation" << endl;
        ss << "'Send Data via Osc' >> Activate Osc stream" << endl;
        ss << "'Send Tanslation' >> Whether to send rotations or translations of joints" << endl;
        ss << "'Port' >> Port the software is sending joint data of skeleton via Osc" << endl;
        ss << "'Host' >> IP address the software is sending joint data via Osc (Default 'localhost')" << endl;
        ss << "'Port' >> Port the software is sending joint data of skeleton via Osc" << endl;
        ss << "'Receiver Port' >> Port the software is listening for Gyro data (Default 6565)" << endl;
        ss << "'Toggle Fullscreen' >> Guess what!" << endl;
        ss << "'Gyros View' >> Switch over to Gyro View Port" << endl;
        ss << endl;
        ofDrawBitmapString(ss.str().c_str(), width * 0.05 + 20, height * 0.05 + 20);
        drawInteractionArea();
        ofSetColor(255);

    }
    // If "g" is pressed, guide text will appear
    if (bGuide == true) {
        bHelp = false;
        bAbout = false;
        ofFill();
        ofSetColor(50, 50, 50, 255);
        ofDrawRectangle(width * 0.05, height * 0.05, width * 0.9, height * 0.9);
        ofSetColor(255, 200, 0);
        stringstream ss;
        ss << "Chordata Mover >> Guide ===================================================================================="<<endl;
        ss << endl;
        ss << "The following guide will show how to use this software with a Chordata Motion Capture System" << endl;
        ss << endl;
        ss << "GYRO VIEW ==================================================================================================="<<endl;
        ss << endl;
        ss << ">> Simple representation of the quaternions coming direct from the gyroscopes. Offset will set the all"<< endl;
        ss << "   arrows in a zero position. Now, little errors or irritations of the gyroscope can be examined."<< endl;
        ss << endl;
        ss << ">> Use 'Show Gyro Data' to see the full data stream of each gyro and its body position"<< endl;
        ss << endl;
        ss << endl;
        ss << "3D VIEW ====================================================================================================="<<endl;
        ss << endl;
        ss << ">> The 3D viewport show a virtual representation of the performer. There is a gui and a timeline (w.i.p.)." << endl;
        ss << endl;
        ss << ">> In the gui settings can saved and imported, accordingly to the performer file from 'Chordata Sizer'" << endl;
        ss << endl;
        ss << ">> The offset of all gyros can be delayed to give the performer time to go into start position."<< endl;
        ss << endl;
        ss << ">> The Osc settings makes it possible stream position or translation data from all joints to another "<< endl;
        ss << "   application. The option 'Send Translation' will send the global positions of each joint instead rotations."<< endl;
        ss << endl;
        ss << ">> The 'Receiver Port' option in 'GENERAL SETTINGS' makes it possible to run 2 or more 'Chordata Mover' at "<< endl;
        ss << "   the same time (for 2 or more performer). "<< endl;
        ss << endl;
        ss << endl;
        ss << "TIMELINE ===================================================================================================="<<endl;
        ss << endl;
        ss << ">> Timeline is work in progress! It is planned to add recording, playing and editing of motions in future"<< endl;
        ss << "   releases. "<<endl;
        ss << endl;
        ofDrawBitmapString(ss.str().c_str(), width * 0.05 + 20, height * 0.05 + 20);
        ofSetColor(255);
    }
    // If "a" is pressed, guide text will appear
    if (bAbout == true) {
        bGuide = false;
        bHelp = false;
        ofFill();
        ofSetColor(50, 50, 50, 255);
        ofDrawRectangle(width / 2 - width * 0.3, height / 2 - height * 0.3, width * 0.6, height * 0.3);
        ofSetColor(0, 200, 200);
        stringstream ss;
        ss << "Chordata Mover >> About" << endl;
        ss << endl;
        ss << "Version v0.1" << endl;
        ss << endl;
        ss << "Software developed by Jens Meisner (www.jensmeisner.net)" << endl;
        ss << endl;
        ss << "Licensed under GNU GENERAL PUBLIC LICENSE 3.0" << endl;
        ss << endl;
        ss << "contact@jensmeisner.net" << endl;
        ss << endl;
        ofDrawBitmapString(ss.str().c_str(), width / 2 - width * 0.3 + 60, height / 2 - height * 0.3 + 60);
        ofSetColor(255);
    }
}

//-------------------------------------------------------------
void ofApp::grid(int size, int count){
    ofSetColor(gridColor);
    int gridsize = size * count;
    for(int i = 0; i < count + 1; i++)
    {
        ofDrawLine(-gridsize/2 + (size * i), -gridsize/2, -gridsize/2 + (size * i), gridsize/2);
    }
    for (int j = 0; j < count + 1; j++)
    {
        ofDrawLine(-gridsize/2, -gridsize/2 + (size * j), gridsize/2, -gridsize/2 + (size * j));
    }
    ofSetColor(255);
}


//--------------------------------------------------------------
void ofApp::oscMessage(){
    // Check for waiting messages
    while(receiver.hasWaitingMessages()){
        // Get the next message
        ofxOscMessage m;
        receiver.getNextMessage(m);
        // Check for mouse moved message
        //cout << m << endl;
        ///Chordata/<property>/<name_of_the_bone>
        ///Chordata/q/kc-CH_2-2: (0.9914214611053467, 0.0532936230301857, -0.016414398327469826, 0.11994471400976181)
        /// /Chordata/q/kc-CH_2-2: (w, x, y, z)
        if(m.getAddress() == "/%/kc-CH_6-2"){
            // RightFoot
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[0].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[0].x() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[0].z() = -(m.getArgAsFloat(i));
                }
                else if(i == 3){
                    kc_quats[0].y() = -(m.getArgAsFloat(i));
                }
            }
        }
        else if(m.getAddress() == "/%/kc-CH_6-1"){
            // RightLeg
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[1].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[1].x() = (m.getArgAsFloat(i));
                }
                else if(i == 2){
                    kc_quats[1].z() = (m.getArgAsFloat(i));
                }
                else if(i == 3){
                    kc_quats[1].y() = (m.getArgAsFloat(i));
                }
            }
            rotateAxis(1, 0, 0, -90.0);
            kc_quats[1] = kc_quats[1] * rotAxis;
        }
        else if(m.getAddress() == "/%/kc-CH_6-0"){
            // RightUpLeg
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[2].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[2].z() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[2].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[2].x() = m.getArgAsFloat(i);
                }
            }
        }
        else if(m.getAddress() == "/%/kc-CH_5-0"){
            // Hips
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[3].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[3].x() = -(m.getArgAsFloat(i));
                }
                else if(i == 2){
                    kc_quats[3].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[3].z() = m.getArgAsFloat(i);
                }
            }
        }
        else if(m.getAddress() == "/%/kc-CH_4-2"){
            // LeftFoot
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[4].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[4].x() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[4].z() = -(m.getArgAsFloat(i));
                }
                else if(i == 3){
                    kc_quats[4].y() = -(m.getArgAsFloat(i));
                }
            }
        }
        else if(m.getAddress() == "/%/kc-CH_4-1"){
            // LeftLeg
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[5].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[5].x() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[5].z() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[5].y() = m.getArgAsFloat(i);
                }
            }
            rotateAxis(1, 0, 0, -90.0);
            kc_quats[5] = kc_quats[5] * rotAxis;
        }
        else if(m.getAddress() == "/%/kc-CH_4-0"){
            // LeftUpLeg
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[6].w() = -m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[6].x() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[6].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[6].z() = m.getArgAsFloat(i);
                }
            }
            rotateAxis(1, 0, 1, -90.0);
            kc_quats[6] = kc_quats[6] * rotAxis;
        }
        else if(m.getAddress() == "/%/kc-CH_3-2"){
            // RightHand
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[7].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[7].z() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[7].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[7].x() = m.getArgAsFloat(i);
                }
            }
        }
        else if(m.getAddress() == "/%/kc-CH_3-1"){
            // RightForeArm
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[8].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[8].z() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[8].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[8].x() = m.getArgAsFloat(i);
                }
            }
        }
        else if(m.getAddress() == "/%/kc-CH_3-0"){
            // RightArm
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[9].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[9].z() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[9].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[9].x() = m.getArgAsFloat(i);
                }
            }
        }
        else if(m.getAddress() == "/%/kc-CH_2-2"){
            // Head
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[10].w() = -m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[10].x() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[10].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[10].z() = m.getArgAsFloat(i);
                }
            }
            rotateAxis(0, 1, 0, -90.0);
            kc_quats[10] = kc_quats[10] * rotAxis;
        }
        else if(m.getAddress() == "/%/kc-CH_2-1"){
            // Chest
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[11].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[11].x() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[11].z() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[11].y() = m.getArgAsFloat(i);
                }
            }
            rotateAxis(1, 0, 0, 90.0);
            kc_quats[11] = kc_quats[11] * rotAxis;
        }
        else if(m.getAddress() == "/%/kc-CH_1-2"){
            // LeftHand
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[12].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[12].z() = -m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[12].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[12].x() = -m.getArgAsFloat(i);
                }
            }
        }
        else if(m.getAddress() == "/%/kc-CH_1-1"){
            // LeftForeArm
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[13].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[13].z() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[13].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[13].x() = m.getArgAsFloat(i);
                }      
            }
            rotateAxis(0, 1, 0, 180.0);
            kc_quats[13] = kc_quats[13] * rotAxis;
        }
        else if(m.getAddress() == "/%/kc-CH_1-0"){
            // LeftArm
            for(int i = 0; i < int(m.getNumArgs()); i++){
                if(i == 0){
                    kc_quats[14].w() = m.getArgAsFloat(i);
                }
                else if(i == 1){
                    kc_quats[14].z() = m.getArgAsFloat(i);
                }
                else if(i == 2){
                    kc_quats[14].y() = m.getArgAsFloat(i);
                }
                else if(i == 3){
                    kc_quats[14].x() = m.getArgAsFloat(i);
                }
            }
            rotateAxis(0, 1, 0, -180.0);
            kc_quats[14] = kc_quats[14] * rotAxis;
        }
        //Change the rotation of current rotation of gyros
        for(int i = 0; i < gyrosCount; i++)
        {
            if(bGyros)
            {
                quatNodes[i].setOrientation(kc_quats[i].inverse() * zeroDiffG[i]);
            }
            else if(bThreeD)
            {
                quatNodes[i].setOrientation(kc_quats[i].inverse() * zeroDiffS[i]);
            }
        }
    }
}

//--------------------------------------------------------------
void ofApp::drawInteractionArea(){
    ofRectangle vp = ofGetCurrentViewport();
    float r = std::min<float>(vp.width, vp.height) * 0.5f;
    float x = vp.width * 0.5f;
    float y = vp.height * 0.5f;
    ofPushStyle();
    ofSetLineWidth(1);
    ofSetColor(0, 255, 0, 125);
    ofNoFill();
    glDepthMask(false);
    ofDrawCircle(x, y, r);
    glDepthMask(true);
    ofPopStyle();
}

//--------------------------------------------------------------
void ofApp::gyro(int size){
    // Set offset 0 point and end points in all 3 directions
    spoint = ofVec3f(0,0,0);
    epoint_x = ofVec3f(size,0,0);
    epoint_y = ofVec3f(0,size,0);
    epoint_z = ofVec3f(0,0,size);
    hsize = 2.0;
    // Set color and draw arrow one for each direction
    ofSetColor(255,0,0);
    ofDrawArrow(spoint,epoint_x,hsize);
    ofSetColor(0,255,0);
    ofDrawArrow(spoint,epoint_y,hsize);
    ofSetColor(0,0,255);
    ofDrawArrow(spoint,epoint_z,hsize);
    ofSetColor(255,255,255);
}

//--------------------------------------------------------------
void ofApp::offsetGyros(){

    if(bGyros == true)
    {
        for(int i = 0; i < gyrosCount; i++)
        {
            //Calculate the difference between the last offset differences and reset
            zeroDiffG[i] = kc_quats[i] * zeroG[i].inverse();
            zeroDiffG[i].normalize();
        }
    }
    if(bThreeD == true)
    {
        for(int i = 0; i < gyrosCount; i++)
        {
            //Calculate the difference between the last offset differences and reset
            zeroDiffS[i] = kc_quats[i] * zeroS[i].inverse();
            zeroDiffS[i].normalize();
        }
        if(floorFix == true) {
            fixPoint.set(offset);
            setFixPoint = false;
            fixedJoint = 0;
            Skeleton[jLbls[0]]->setGlobalPosition(Hips + offset);
            floorFix = true;
            floorDist.set(0, 0);
            fixPoint.set(0, 0, 0);
        }
    }
}

//--------------------------------------------------------------
void ofApp::rotateAxis(const double &xx, const double &yy, const double &zz, const double &a){
    // Calculate sin(theta / 2) once for optimization
    double factor = sin(a / 2.0);

    // Calculate the x, y and z of the quaternion
    double x = xx * factor;
    double y = yy * factor;
    double z = zz * factor;

    // Calcualte the w value by cos( theta / 2 )
    double w = cos( a / 2.0 );

    rotAxis.set(x, y, z, w);
    rotAxis.normalize();
}

//--------------------------------------------------------------
void ofApp::rotateSkeleton(){
    //Updates orientation values of joints with gyros
    for(int i = 0; i < int(joints.size()); i++){
        if(i == 12){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[0].getOrientationQuat());
        }
        else if(i == 11){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[1].getOrientationQuat());
        }
        else if(i == 10){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[2].getOrientationQuat());
        }
        else if(i == 0){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[3].getOrientationQuat());
        }
        else if(i == 8){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[4].getOrientationQuat());
        }
        else if(i == 7){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[5].getOrientationQuat());
        }
        else if(i == 6){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[6].getOrientationQuat());
        }
        else if(i == 22){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[7].getOrientationQuat());
        }
        else if(i == 21){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[8].getOrientationQuat());
        }
        else if(i == 20){
            if(ra2rsVal > 0)
            {
                //Add rotation
                //Skeleton[jLbls[i]]->setOrientation(quatNodes[12].getOrientationQuat());
                //Add rotation to
                //Skeleton[jLbls[i - 1]]->setOrientation(quatNodes[12].getOrientationQuat());
            }
            else {
                Skeleton[jLbls[19]]->setOrientation(offsetRot);
                Skeleton[jLbls[i]]->setOrientation(quatNodes[9].getOrientationQuat());
            }
        }
        else if(i == 4){
            if(h2nVal > 0)
            {
                Skeleton[jLbls[3]]->setOrientation(offsetRot);
                Skeleton[jLbls[i]]->setOrientation(quatNodes[10].getOrientationQuat());
                //Add rotation to Head joint
                //Skeleton[jLbls[i]]->setOrientation(quatNodes[10].getOrientationQuat());
                //Add rotation to Neck joint
                //Skeleton[jLbls[i - 1]]->setOrientation(quatNodes[10].getOrientationQuat());
            }
            else {
                Skeleton[jLbls[3]]->setOrientation(offsetRot);
                Skeleton[jLbls[i]]->setOrientation(quatNodes[10].getOrientationQuat());
            }
        }
        else if(i == 2){
            if(c2sVal > 0)
            {
                Skeleton[jLbls[1]]->setOrientation(offsetRot);
                Skeleton[jLbls[i]]->setOrientation(quatNodes[11].getOrientationQuat());
                //Add rotation to Chest joint
                //Skeleton[jLbls[i]]->setOrientation(quatNodes[11].getOrientationQuat());
                //Add rotation to Spine joint
                //Skeleton[jLbls[i - 1]]->setOrientation(quatNodes[11].getOrientationQuat());

            }
            else {
                Skeleton[jLbls[1]]->setOrientation(offsetRot);
                Skeleton[jLbls[i]]->setOrientation(quatNodes[11].getOrientationQuat());
            }
        }
        else if(i == 17){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[12].getOrientationQuat());
        }
        else if(i == 16){
            Skeleton[jLbls[i]]->setOrientation(quatNodes[13].getOrientationQuat());
        }
        else if(i == 15){
            if(la2lsVal > 0)
            {
                //Add rotation
                //Skeleton[jLbls[i]]->setOrientation(quatNodes[12].getOrientationQuat());
                //Add rotation to
                //Skeleton[jLbls[i - 1]]->setOrientation(quatNodes[12].getOrientationQuat());
            }
            else {
                Skeleton[jLbls[14]]->setOrientation(offsetRot);
                Skeleton[jLbls[i]]->setOrientation(quatNodes[14].getOrientationQuat());
            }
        }
        //cout<<Skeleton[jLbls[i]]->getOrientation()<<endl;
    }
}

// Load file dialog ------------------------------------------------
void ofApp::processOpenFileSelection(ofFileDialogResult openFileResult){
    // Get name and path of the file to be open
    ofLogVerbose("getName(): "  + openFileResult.getName());
    ofLogVerbose("getPath(): "  + openFileResult.getPath());
    //Set the file call
    ofFile file (openFileResult.getPath());
    // Proceed loading file, if file exists
    if (file.exists()){
        ofLogVerbose("The file exists - now checking the type via file extension");
        string fileExtension = ofToUpper(file.getExtension());
        // Check, if file has xml extension
        if(fileExtension == "XML")
        {
            // Save the file extension to use when we save out
            originalFileExtension = fileExtension;
            // Load xml file
            XML.loadFile(openFileResult.getPath());
        }
    }
}

//--------------------------------------------------------------
void ofApp::portInChanged(string& port){
    portIn = ofToInt(port);
    ofLog() << "listening for osc messages on port " << portIn;
    receiver.setup(portIn);
}

//--------------------------------------------------------------
void ofApp::portOutChanged(string& port){
    // Reset sender when port changed
    portOut = ofToInt(port);
    sender.clear();
    sender.setup(host, portOut);
}

//--------------------------------------------------------------
void ofApp::hostChanged(string& hostIn){
    // Reset sender when host changed
    host = hostIn;
    sender.clear();
    sender.setup(host, portOut);
}

//--------------------------------------------------------------
void ofApp::sendingRotation(){
    // Get each bone and send rotation data
    for(int i = 0; i < int(joints.size()); i++)
    {
        send.setAddress(jLbls[i]);
        send.addFloatArg(Skeleton[jLbls[i]]->getOrientation().x());
        send.addFloatArg(Skeleton[jLbls[i]]->getOrientation().y());
        send.addFloatArg(Skeleton[jLbls[i]]->getOrientation().z());
        send.addFloatArg(Skeleton[jLbls[i]]->getOrientation().w());
        sender.sendMessage(send, false);
        //cout<<send<<endl;
        send.clear();
    }
    sendT.setAddress("HipsT");
    sendT.addFloatArg(Skeleton["Hips"]->getGlobalPosition().x);
    sendT.addFloatArg(Skeleton["Hips"]->getGlobalPosition().y);
    sendT.addFloatArg(Skeleton["Hips"]->getGlobalPosition().z);
    sender.sendMessage(sendT, false);
    //cout<<sendT<<endl;
    sendT.clear();
}

//--------------------------------------------------------------
void ofApp::sendingTranslation(){
    // Get each bone and send translation data
    for(int i = 0; i < int(joints.size()); i++)
    {
        send.setAddress(jLbls[i]);
        send.addFloatArg(Skeleton[jLbls[i]]->getGlobalPosition().x);
        send.addFloatArg(Skeleton[jLbls[i]]->getGlobalPosition().y);
        send.addFloatArg(Skeleton[jLbls[i]]->getGlobalPosition().z);
        sender.sendMessage(send, false);
        //cout<<send<<endl;
        send.clear();
    }
}

//--------------------------------------------------------------
void ofApp::floorContact(int joint){
    //Calculate new position and apply it from fixed joint to hip
    newPos = Skeleton[jLbls[joint]]->getGlobalPosition();
    cout<<"NewPos: " + ofToString(newPos)<<endl;
    newPos = curPos - newPos + fixPoint;
    hipsPos.set(newPos + offset);
    Skeleton[jLbls[0]]->setGlobalPosition(hipsPos);
    curPos = newPos;
    cout<<"HipPos: " + ofToString(hipsPos)<<endl;
}

//--------------------------------------------------------------
void ofApp::delayOffset(string& offset){
    //Set offset time from string
    offsetTD = ofToInt(offset);
}

//--------------------------------------------------------------
void ofApp::offsetCountDown(){
    //

    if(offsetTD > 0)
    {
        if(startCount == true)
        {
            startT = ofGetElapsedTimeMillis();
            countDown = offsetTD;
            counting = true;
            startCount = false;

        }
        curT = ofGetElapsedTimeMillis() - startT;
        if(curT > (offsetTD + 1 - countDown) * 1000 && countDown >= 0)
        {
            countDown = countDown - 1;
        }
        else if(countDown < 0)
        {
            offsetGyros();
            startCount = true;
            counting = false;
        }
    }
    else{
        offsetGyros();
    }
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch(key) {
        case 'z':
        case 'Z':
            // Offset all rotation data
            offsetGyros();
            cout<<"Rotational offset: Done"<<endl;
        break;
        case 'M':
        case 'm':
            bGuiG ^= true;
            bGui3D ^= true;
            break;
        case 'H':
        case 'h':
            bHelp = !bHelp;
            break;
        case 'G':
        case 'g':
            bGuide = !bGuide;
            break;
        case 'F':
        case 'f':
            tglFull = !tglFull;
        break;
        case 'A':
        case 'a':
            bAbout = !bAbout;
            break;
        case 'I':
        case 'i':
        if (key == 'i' || key == 'I'){
            importPerformerFile();
        }
            break;
        case 'L':
        case 'l':
            if(bLive == false) { bLive = true; }
            else { bLive = false; }
        break;
        case 'T':
        case 't':
            tlFullscreen = !tlFullscreen;
        break;
        case '1':
            if(bGyros == false)
            {
                bGyros = true;
                gyrosCam.enableMouseInput();
                if(bThreeD == true)
                {
                    bThreeD = false;
                    threeDCam.disableMouseInput();
                }
            }
            break;
        case '2':
            if(bThreeD == false)
            {
                bThreeD = true;
                threeDCam.enableMouseInput();
                if(bGyros == true)
                {
                    bGyros = false;
                    gyrosCam.disableMouseInput();
                }
            }
            break;
    }
    if (key == OF_KEY_UP)
    {
        // Scroll through reference points upwards
        if(bGyros == true && gyroData)
        {
            curJoint = curJoint + 1;
            if (curJoint > int(gLbls.size() - 1))
            {
                curJoint = 0;
            }
        }
    }
    if (key == OF_KEY_DOWN)
    {
        // Scroll through reference points downwards
        if(bGyros == true && gyroData)
        {
            curJoint = curJoint - 1;
            if (curJoint < 0)
            {
                curJoint = int(gLbls.size()) - 1;
            }
        }
    }
}

//--------------------------------------------------------------
void ofApp::importPerformerFile(){
    // Open the Open File Dialog
    ofFileDialogResult openFileResult= ofSystemLoadDialog("Select PERFORMER xml file");
    // Check if the user opened a file
    if (openFileResult.bSuccess){
        ofLogVerbose("User selected a file");
        // We have a file, check it and process it
        processOpenFileSelection(openFileResult);
        fileSizer = openFileResult.filePath;
        cout<<fileSizer<<endl;
        // Get ratios for imported file
        XML.pushTag("OFFSET");
        offset.x = XML.getValue("X", 0.0);
        offset.y = XML.getValue("Y", 0.0);
        offset.z = XML.getValue("Z", 0.0);
        XML.popTag();
        XML.pushTag("RATIO");
        refRatioX = XML.getValue("X", 0.0);
        refRatioY = XML.getValue("Y", 0.0);
        refRatioZ = XML.getValue("Z", 0.0);
        XML.popTag();
        XML.pushTag("SKELETON");
        // Go through all joint references and add x, y, z to added joint name
        for(int i = 0; i < int(joints.size()); i++)
        {
            XML.pushTag(jLbls[i]);
            joints[i].x = int(XML.getValue("X", 0.0) / refRatioX + offset.x);
            joints[i].y = int(XML.getValue("Y", 0.0)  / refRatioY + offset.y);
            joints[i].z = int(XML.getValue("Z", 0.0)  / refRatioZ + offset.z);
            XML.popTag();
        }
        XML.popTag();
    }else {
        ofLogVerbose("User hit cancel");
    }

}

//--------------------------------------------------------------
void ofApp::importSettingsFile(){
    // Open the Open File Dialog
    ofFileDialogResult openFileResult= ofSystemLoadDialog("Select SETTINGS xml file");
    // Check if the user opened a file
    if (openFileResult.bSuccess){
        ofLogVerbose("User selected a file");
        // We have a file, check it and process it
        processOpenFileSelection(openFileResult);
        // Get settings from imported file
        XML.pushTag("SETTINGS");
        XML.pushTag("SIZERFILE");
        fileSizer = XML.getValue("LOCATION", "");
        XML.popTag();
        XML.pushTag("OFFSETDELAY");
        strOffsetTD = XML.getValue("DELAY", "");
        XML.popTag();
        XML.pushTag("FLOORTHRES");
        floorThres = XML.getValue("THRESHOLD", 0.0);
        XML.popTag();
        XML.pushTag("SENDOSC");
        sending = XML.getValue("SENDING", 0);
        XML.popTag();
        XML.pushTag("SENDTRANS");
        tglTrans = XML.getValue("SENDING", 0);
        XML.popTag();
        XML.pushTag("SENDHOST");
        host = XML.getValue("HOST", "");
        XML.popTag();
        XML.pushTag("SENDPORT");
        portOut = XML.getValue("PORT", 0);
        XML.popTag();
        XML.pushTag("RECPORT");
        portIn = XML.getValue("PORT", 0);
        XML.popTag();
        XML.popTag();
    }else {
        ofLogVerbose("User hit cancel");
    }
}

//--------------------------------------------------------------
void ofApp::exportSettingsFile(){
    // Write all joint positions and cube references in converted millimeters into a XML file and save it
    ofFileDialogResult saveFileResult = ofSystemSaveDialog(ofGetTimestampString() + "." + ofToLower(originalFileExtension), "Save your SETTINGS file");
    if (saveFileResult.bSuccess){
        // Clear out XML file
        XML.clear();
        // Add Root tag
        XML.addTag("SETTINGS");
        XML.pushTag("SETTINGS");
        XML.addTag("SIZERFILE");
        XML.pushTag("SIZERFILE");
        XML.addValue("LOCATION", fileSizer);
        XML.popTag();
        XML.addTag("OFFSETDELAY");
        XML.pushTag("OFFSETDELAY");
        XML.addValue("DELAY", strOffsetTD);
        XML.popTag();
        XML.addTag("FLOORTHRES");
        XML.pushTag("FLOORTHRES");
        XML.addValue("THRESHOLD", floorThres);
        XML.popTag();
        XML.addTag("SENDOSC");
        XML.pushTag("SENDOSC");
        XML.addValue("SENDING", sending);
        XML.popTag();
        XML.addTag("SENDTRANS");
        XML.pushTag("SENDTRANS");
        XML.addValue("SENDING", tglTrans);
        XML.popTag();
        XML.addTag("SENDHOST");
        XML.pushTag("SENDHOST");
        XML.addValue("HOST", host);
        XML.popTag();
        XML.addTag("SENDPORT");
        XML.pushTag("SENDPORT");
        XML.addValue("PORT", portOut);
        XML.popTag();
        XML.addTag("RECPORT");
        XML.pushTag("RECPORT");
        XML.addValue("PORT", portIn);
        XML.popTag();
        XML.popTag();
        // Save all to the file
        XML.saveFile(saveFileResult.filePath);
    }
}

//--------------------------------------------------------------
void ofApp::loadDefault(){
    XML.loadFile(fileSettings);
    XML.pushTag("SETTINGS");
    XML.pushTag("SIZERFILE");
    fileSizer = XML.getValue("LOCATION", "");
    XML.popTag();
    XML.pushTag("OFFSETDELAY");
    strOffsetTD = XML.getValue("DELAY", "");
    XML.popTag();
    XML.pushTag("FLOORTHRES");
    floorThres = XML.getValue("THRESHOLD", 0.0);
    XML.popTag();
    XML.pushTag("SENDOSC");
    sending = XML.getValue("SENDING", 0);
    XML.popTag();
    XML.pushTag("SENDTRANS");
    tglTrans = XML.getValue("SENDING", 0);
    XML.popTag();
    XML.pushTag("SENDHOST");
    host = XML.getValue("HOST", "");
    XML.popTag();
    XML.pushTag("SENDPORT");
    portOut = XML.getValue("PORT", 0);
    XML.popTag();
    XML.pushTag("RECPORT");
    portIn = XML.getValue("PORT", 0);
    XML.popTag();
    XML.popTag();
}

//--------------------------------------------------------------
void ofApp::fullscreenButtonPressed(){
    tglFull = !tglFull;
}

//--------------------------------------------------------------
void ofApp::viewButtonPressed(){
    if(bGyros == false)
    {
        bGyros = true;
        bThreeD = false;
        threeDCam.disableMouseInput();
        gyrosCam.enableMouseInput();
    }
    else if(bThreeD == false)
    {
        bGyros = false;
        bThreeD = true;
        gyrosCam.disableMouseInput();
        threeDCam.enableMouseInput();
    }
}

////--------------------------------------------------------------
//void ofApp::exit(){

//}

