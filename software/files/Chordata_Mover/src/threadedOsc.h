#pragma once


#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxSkeleton.h"
#include <atomic>

/// This is a simple example of a ThreadedObject created by extending ofThread.
/// It contains data (count) that will be accessed from within and outside the
/// thread and demonstrates several of the data protection mechanisms (aka
/// mutexes).
class ThreadedOsc: public ofThread
{
public:
    /// On destruction wait for the thread to finish
    /// so we don't destroy the pixels while they are
    /// being used. Otherwise we would crash
    ~ThreadedOsc(){
        stop();
        waitForThread(false);
    }

    void setup(int jsize, string h, int p){
        jointsize = jsize;
        host = h;
        port = p;
        sender.setup(h, p);
        start();
    }

    /// Start the thread.
    void start(){
        startThread();
    }

    /// Signal the thread to stop.  After calling this method,
    /// isThreadRunning() will return false and the while loop will stop
    /// next time it has the chance to.
    /// In order for the thread to actually go out of the while loop
    /// we need to notify the condition, otherwise the thread will
    /// sleep there forever.
    /// We also lock the mutex so the notify_all call only happens
    /// once the thread is waiting. We lock the mutex during the
    /// whole while loop but when we call condition.wait, that
    /// unlocks the mutex which ensures that we'll only call
    /// stop and notify here once the condition is waiting
    void stop(){
        std::unique_lock<std::mutex> lck(mutex);
        stopThread();
        condition.notify_all();
    }

    /// Everything in this function will happen in a different
    /// thread which leaves the main thread completelty free for
    /// other tasks.
    void threadedFunction(){
        while(isThreadRunning()){
			// Lock the mutex until the end of the block, until the closing }
			// in which this variable is contained or we unlock it explicitly
            std::unique_lock<std::mutex> lock(mutex);

            // The mutex is now locked so we can modify
            // the shared memory without problem
            if(sendTrans == true)
            {
                ofxOscMessage send;
                send.setAddress(address);
                send.addFloatArg(translation.x);
                send.addFloatArg(translation.y);
                send.addFloatArg(translation.z);
                sender.sendMessage(send, false);
                send.clear();
            }
            else{
                ofxOscMessage send;
                send.setAddress(address);
                send.addFloatArg(orientation.w());
                send.addFloatArg(orientation.x());
                send.addFloatArg(orientation.y());
                send.addFloatArg(orientation.z());
                sender.sendMessage(send, false);
                cout<<send<<endl;
                send.clear();
            }
            // Now we wait for the main thread to finish
            // with this frame until we can generate a new one
            // This sleeps the thread until the condition is signaled
            // and unlocks the mutex so the main thread can lock it
            condition.wait(lock);
        }
    }

    void changePort(int p)
    {
        port = p;
        sender.clear();
        sender.setup(host, port);
    }

    void changeHost(string h)
    {
        host = h;
        sender.clear();
        sender.setup(host, port);
    }

    void setAddress(string a)
    {
        //std::unique_lock<std::mutex> lock(mutex);
        send.setAddress(a);
        condition.notify_all();
    }

    void updateRot(string a, ofQuaternion quat){
        //std::unique_lock<std::mutex> lock(mutex);
        address = a;
        orientation = quat;
        condition.notify_all();
    }

    void updateTrans(string a, ofVec3f t){
        address = a;
        send.setAddress(a);
        translation = t;
        std::unique_lock<std::mutex> lock(mutex);
        condition.notify_all();
    }

    void sendTranslation(bool t){
        sendTrans = t;
    }

    string getMessageInfo()
    {
        return ofToString(send);
    }

protected:
    vector<ofNode> joints;
    ofQuaternion orientation;
    ofVec3f translation;
    int port, jointsize;
    bool sendTrans = false;
    string host, address;
    ofxOscSender sender;
    ofxOscMessage send;
    std::condition_variable condition;
};

