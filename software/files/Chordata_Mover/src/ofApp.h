#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxXmlSettings.h"
#include "ofxSkeleton.h"
#include "ofxGui.h"
#include "ofxLineaDeTiempo.h"

//Threaded Objects


// listening port
//#define PORT 6565

// max number of strings to display
#define NUM_MSG_STRINGS 80

#include <iostream>
#include <sstream>
using namespace std;

class ofApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();

    void oscMessage();
    void grid(int size, int count);
    void gyro(int size);
    void bone(int x1, int y1, int z1, int x2, int y2, int z2, int size, ofColor color, bool bWire);
    void offsetGyros();
    void rotateAxis(const double &xx, const double &yy, const double &zz, const double &a);
    void rotateSkeleton();
    void processOpenFileSelection(ofFileDialogResult openFileResult);
    void portInChanged(string& port);
    void portOutChanged(string& port);
    void hostChanged(string& hostIn);
    void sendingRotation();
    void sendingTranslation();
    void floorContact(int j);
    void delayOffset(string& offset);
    void offsetCountDown();
    void keyPressed(int key);
    void importPerformerFile();
    void importSettingsFile();
    void exportSettingsFile();
    void loadDefault();
    void fullscreenButtonPressed();
    void viewButtonPressed();
    //void exit();

    // Osc channel to receive data from Notochord software
    ofxOscReceiver receiver;
    ofxOscSender sender;
    ofVec3f tData;
    ofxOscMessage send, sendT;

    // Import font
    ofTrueTypeFont	zekton, zektonCount, zektonGyro;
    // Help text, guide text, about text
    bool bHelp, bGuide, bAbout;
    // Export/Import XML performer file from Chordata Sizer
    ofxXmlSettings XML;
    int refDist;
    float refRatioX, refRatioY, refRatioZ, fThres;
    int width, height, portIn, portOut;
    string pIn, pOut, host, fileSettings, fileSizer;
    bool bLive, tglTrans, sending;
    ofQuaternion rotAxis;

    // Gui ================================================
    ofxPanel guiG, gui3D;
    ofxGuiGroup perfGroup, generalGroup, oscGroup;
    bool bGuiG, bGui3D;
    ofxLabel lGuiG, lGui3D, gValues, recLbl, oscLbl, pref3D, prefG, perf3D;
    int guiWidth, guiHeight, h2nVal, c2sVal, la2lsVal, ra2rsVal;
    ofxButton zeroBtn3D, zeroBtnG, viewBtnG, viewBtn3D, importSize, newRec;
    ofxButton exportRec, importRec, tglFullG, tglFull3D, importSettings, exportSettings;
    //text inpurt port, waitStartRec, timeOfRec;
    ofxFloatSlider floorThres;
    ofxIntSlider head2Neck, rArm2rShoulder, lArm2lShoulder, chest2Spine;
    ofxToggle tglFullscreen, gyroData, record, play, sendTrans, sendOsc, floorFix, goLive;
    ofParameter<string> portInInput, portOutInput, hostInput, offsetDelay;

    // Gyro View ========================================
    bool bGyros;
    // Quaternions data nodes of KC Sensors
    vector<ofQuaternion> kc_quats, zeroDiffG, zeroDiffS, zeroG, zeroS;
    ofQuaternion kc_quaternion, diff_quaternion;
    vector<ofNode> quatNodes;
    ofNode gRot, rotDiff, posDiff;
    ofVec3f rotVec;
    // Vector to automate
    int gyrosCount, gyronumber, gridsize, curJoint;
    vector<ofVec3f> gyros;
    // Text for labeling arrows
    vector<string> gLbls, gjLbls;
    // 3D view port
    void drawInteractionArea();
    ofEasyCam gyrosCam; // add mouse controls for camera movement
    // Arrow & Gird
    int size, dist, gapSize, lineCount;
    glm::vec3 spoint, epoint_x, epoint_y, epoint_z;
    float hsize;
    string title;
    //Zero rotation data
    bool tglZero, gData;
    ofColor gridColor, lblColor, colorSelect;

    // 3D View ========================================
    bool bThreeD;
    // Vector for joint labels
    vector <string> jLbls;
    vector <ofNode> skeleton;
    bool fContact, setFixPoint, startCount, counting;
    ofEasyCam threeDCam;
    // Joint positions in x, y, z
    ofVec3f Hips, Spine, Chest;
    ofVec3f LeftUpLeg, LeftLeg, LeftFoot, LeftFootEnd;
    ofVec3f RightUpLeg, RightLeg, RightFoot, RightFootEnd;
    ofVec3f LeftShoulder, LeftArm, LeftForeArm, LeftHand, LeftHandEnd;
    ofVec3f RightShoulder, RightArm, RightForeArm, RightHand, RightHandEnd;
    ofVec3f Neck, Head, HeadEnd;
    vector <ofVec3f> joints;
    typedef shared_ptr<pal::ofxSkeleton::ofxJoint>  JointP_t;
    map<string, JointP_t>							Skeleton;
    ofVec3f offset, fixPoint, fixPointNew, hipsPos, curPos, newPos;
    string originalFileExtension, strOffsetTD;
    int fixedJoint, offsetTD, startT, curT, countDown;
    ofColor gC, tC, oC, pC, fixedJC;
    ofVec2f floorDist;
    ofQuaternion offsetRot;

    // Timeline ================================================
    // simply declare an ofxLineaDeTiempo instance.
    ofxLineaDeTiempo timeline = {"Timeline"};
    ofParameterGroup parameters;
    ofParameter<ofVec4f> hips;
    ofParameter<ofColor> color;
    ofxPanel tlGui;
    int tlHeight;
    bool tlFullscreen, tglFull, recording, playing;
};
