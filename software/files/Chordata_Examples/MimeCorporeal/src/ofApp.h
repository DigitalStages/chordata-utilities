#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxFBX.h"
#include "ofxGuiExtended.h"
#include "ofxShadowMap.h"

class ofApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();
    void receiveOsc();
    void offsetGyros();
    void setOrientationQuat(ofVec3f euler);
    void portInChanged(string& port);
    void fullscreenButtonPressed();

    void keyPressed(int key);
    //void exit();

    bool bAbout;
    int width, height;

    // FBX
    ofxFBX fbx1, fbx2;
    int numBones, curJoint;
    ofTrueTypeFont zekton;


    // SCENE
    ofLight light;
    ofVec3f lightP, lightLA;
    ofColor lightC;
    // Shadow
    ofxShadowMap shadowMap;
    ofMaterial groundMaterial;
    float fustrumSize, farClip;
    ofEventListener listener;

    //Camera
    ofEasyCam cam;

    // GUI
    bool bGui, bLive, bFull;
    ofxGui gui;
    ofxGuiPanel* panel;
    ofxGuiGroup *generalS, *sceneS, *oscSettings, *colorSettings, *lightSettings, *customChar;
    ofParameter<bool> goLive, tglAbout, tglFull, enableShadows, offsetBtn, customFbx, showSphere, tweakBoneBtn, tglTweak;
    ofParameter<ofColor> colorBG, colorSphere, colorLight;
    ofParameter<ofFloatColor> colorFloor;
    ofParameter<int> portInInput, importSettingsBtn, exportSettingsBtn;
    ofParameter<ofVec3f> lightPos, lightLookAt;
    ofParameter<ofVec2f> floorSize;
    ofParameter<ofVec3f> boneTweak;
    ofColor gC, sC;

    // OSC
    int portIn;
    string pIn;
    ofxOscReceiver receiver;

    // DATA
    int jointCount;
    ofNode jNode;
    ofQuaternion quat, diff;
    vector<string> jLbls;
    vector<shared_ptr<ofxFBXBone>> bones;
    vector<ofQuaternion> jRot, zeroDiff, zeroOffset;
    ofQuaternion offsetDiff;

    ofVec3f euler;
    ofVec3f hipsT;
    float spaceRatio;
    bool calcRatio;
    float offset;
};
