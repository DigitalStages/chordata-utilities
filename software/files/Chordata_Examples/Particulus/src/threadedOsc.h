#pragma once


#include "ofMain.h"
#include "ofxOsc.h"
#include <atomic>

/// This is a simple example of a ThreadedObject created by extending ofThread.
/// It contains data (count) that will be accessed from within and outside the
/// thread and demonstrates several of the data protection mechanisms (aka
/// mutexes).
class ThreadedOsc: public ofThread
{
public:
    /// On destruction wait for the thread to finish
    /// so we don't destroy the pixels while they are
    /// being used. Otherwise we would crash
    ~ThreadedOsc(){
        stop();
        waitForThread(false);
    }

    void setup(int p){
        port = p;
        receiver.setup(port);
        start();
    }

    /// Start the thread.
    void start(){
        startThread();
    }

    /// Signal the thread to stop.  After calling this method,
    /// isThreadRunning() will return false and the while loop will stop
    /// next time it has the chance to.
    /// In order for the thread to actually go out of the while loop
    /// we need to notify the condition, otherwise the thread will
    /// sleep there forever.
    /// We also lock the mutex so the notify_all call only happens
    /// once the thread is waiting. We lock the mutex during the
    /// whole while loop but when we call condition.wait, that
    /// unlocks the mutex which ensures that we'll only call
    /// stop and notify here once the condition is waiting
    void stop(){
        std::unique_lock<std::mutex> lck(mutex);
        stopThread();
        condition.notify_all();
    }

    /// Everything in this function will happen in a different
    /// thread which leaves the main thread completelty free for
    /// other tasks.
    void threadedFunction(){
        while(isThreadRunning()){
			// Lock the mutex until the end of the block, until the closing }
			// in which this variable is contained or we unlock it explicitly
            //std::unique_lock<std::mutex> lock(mutex);
            while(receiver.hasWaitingMessages()){

                // The mutex is now locked so we can modify
                // the shared memory without problem
                receiver.getNextMessage(recTrans);
                // Check for mouse moved message
                cout << recTrans << endl;
                if(recTrans.getAddress() == "LeftHand")
                {
                    transL.set(recTrans.getArgAsFloat(0), recTrans.getArgAsFloat(1), recTrans.getArgAsFloat(2));
                }
                else if(recTrans.getAddress() == "RightHand")
                {
                    transR.set(recTrans.getArgAsFloat(0), recTrans.getArgAsFloat(1), recTrans.getArgAsFloat(2));
                }
                else if(recTrans.getAddress() == "Hips"){
                    transH.set(recTrans.getArgAsFloat(0), recTrans.getArgAsFloat(1), recTrans.getArgAsFloat(2));
                }

                cout<<transL<<endl;
                cout<<transR<<endl;
                cout<<transH<<endl;
                //recTrans.clear();
            }

            // Now we wait for the main thread to finish
            // with this frame until we can generate a new one
            // This sleeps the thread until the condition is signaled
            // and unlocks the mutex so the main thread can lock it
            //condition.wait(lock);
        }
    }

    void changePort(int p)
    {
        port = p;
        receiver.setup(port);
    }

    ofVec3f getTransL(){
        return transL;
    }

    ofVec3f getTransR(){
        return transR;
    }

    ofVec3f getTransH(){
        return transH;
    }

protected:
    ofVec3f transL, transR, transH;
    int port;
    string address;
    ofxOscReceiver receiver;
    ofxOscMessage recTrans;
    std::condition_variable condition;
};

