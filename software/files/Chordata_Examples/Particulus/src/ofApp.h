#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxFastParticleSystem.h"
#include "ofxXmlSettings.h"
#include "ofxGuiExtended.h"
#include "threadedOsc.h"

class Line{

public:
    ofPoint a;
    ofPoint b;
};

class ofApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();
    void receiveOsc();
    void fullscreenButtonPressed();
    void processOpenFileSelection(ofFileDialogResult openFileResult);
    void importSettingsFile();
    void exportSettingsFile();
    void keyPressed(int key);
    void resetParticleSys();
    void exit();

    bool bAbout, bFull;
    int width, height;
    ofParameter<int> bgR, bgG, bgB;
    string originalFileExtension, fileSettings;
    int w, h;
    ofColor gC, sC;

    // PARTICLE SYSTEM
    int switchId, maxSpeedCircle;
    int cRadius;
    float maxSpeed, cStiffness, cCenterX, cCenterY;
    ofParameter<float> pColorR, pColorG, pColorB, pColorA;

    // OSC
    int portIn;
    ofxOscReceiver receiver;

    // DATA
    int jointCount;
    ofNode jTransL, jTransR;
    ofVec3f posL, posR, posH;
    ofVec2f offsetTrans;

    // Lines
    vector<ofPoint> drawnPointsL, drawnPointsR, drawnPointsH, drawnPoints;
    vector<Line> linesL, linesR, linesH, lines;
    int mouseX, mouseY;
    ofPoint positionL, positionR, positionH;

    // GUI
    bool bGui, bLive, bPort, resetP;
    ofxGui gui;
    ofxGuiPanel* panel;
    ofxGuiGroup *generalS, *sceneS, *bgColor, *partSys, *partColor, *stateOne, *stateTwo, *dlines;
    ofxGuiGroup *lLineColor, *rLineColor, *hLineColor, *oscSettings;
    ofParameter<bool> tglFull, importSettings, exportSettings, bLH, bRH, bH;
    ofParameter<bool> goLive, tglAbout, useMouse, showThreshLine;
    ofParameter<int> circleRadius, lineLength, N, lineDist, bgColorR, bgColorG, bgColorB, portInInput;
    ofParameter<int> lColorR, lColorG, lColorB, lColorA, rColorR, rColorG, rColorB, rColorA, hColorR, hColorG, hColorB, hColorA, switchIdThresh;
    ofParameter<float> maximumSpeed, maximumSpeedCircle, centerStiffness;
    ofParameter<float> particleR, particleG, particleB, particleA;
    ofParameter<ofVec2f> center, centerTrans;
    ofRectangle threshLine;
    ofParameter<int> pCountW, pCountH;

    // XML
    ofxXmlSettings XML;

    // Thread
    ThreadedOsc threadedOsc;

private:
    ofMatrix4x4 projection, modelView;
    FastParticleSystem particles;
    int shaderId;

};

