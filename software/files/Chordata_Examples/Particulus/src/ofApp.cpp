#include "ofApp.h"

const string CIRCLE = "circle";

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFrameRate(30);
    ofSetVerticalSync(false);
    bAbout = false;
    //fileSettings = "settings.xml";

    // PARTICLE SYSTEM
    w = 800;
    h = 800;
    particles.init(w, h, ofPrimitiveMode::OF_PRIMITIVE_POINTS, 2);
    // Positions
    float* particlePosns = new float[w * h * 4];
    for (int y = 0; y < h; ++y)
    {
        for (int x = 0; x < w; ++x)
        {
            unsigned idx = y * w + x;
            particlePosns[idx * 4] =     ofRandom(ofGetWidth());
            particlePosns[idx * 4 + 1] = ofRandom(ofGetHeight());
            particlePosns[idx * 4 + 2] = 0.f;
            particlePosns[idx * 4 + 3] = 0.f;
        }
    }
    particles.loadDataTexture(FastParticleSystem::POSITION, particlePosns);
    delete[] particlePosns;
    // Velocities
    float* velocities = new float [w * h * 4];
    for (int y = 0; y < h; y++){
        for(int x = 0; x < w; x++){
            unsigned idx = y * w + x;
            velocities[idx * 4] =    ofRandom(-1.5, 1.5);
            velocities[idx * 4 +1] = ofRandom(-1.5, 1.5);
            velocities[idx * 4 +2] = 0;
            velocities[idx * 4 +3] = 0;
        }
    }
    particles.loadDataTexture(FastParticleSystem::VELOCITY, velocities);
    delete[] velocities;
    // Update shaders
    particles.addUpdateShader("shaders/updateParticlesRandom");
    particles.addUpdateShader("shaders/updateParticlesCircle", CIRCLE);
    // Draw shader
    particles.addDrawShader("shaders/drawParticles");
    // Matrix
    projection.makeIdentityMatrix();
    projection.set(ofGetCurrentMatrix(OF_MATRIX_PROJECTION));
    projection.preMult(ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
    modelView.makeIdentityMatrix();
    shaderId = 0;
    switchId = ofGetHeight()*0.667;
    pColorR = 0.2;
    pColorB = 0.2;
    pColorG = 0.2;
    pColorA = 0.2;
    maxSpeed = 1.2;
    cRadius = 300;
    cCenterX = ofGetWidth() / 2.0;
    cCenterY = ofGetHeight() / 2.0;
    cStiffness = 0.01;
    maxSpeedCircle = 20;
    // General
    portIn = 6568;
    bGui = false;
    bPort = true;
    //resetP = true;
    gC = ofColor(0, 165, 255);
    sC = ofColor(255, 165, 0);
    bFull = false;
    // Load default setting if exists
    ofFile settings;
    settings.open(fileSettings);
    if(settings.isFile())
    {
        //oscSettings->loadFromFile("settings.xml");
        bgColor->loadFromFile("settings.xml");
        partSys->loadFromFile("settings.xml");
        dlines->loadFromFile("settings.xml");
    }
    else{
        cout<<"Default settings file does not exist."<<endl;
    }
    // GUI
    panel = gui.addPanel("MENU - Press 'm'");
    panel->setPosition(10, 10);
    panel->setWidth(250);
    panel->setBackgroundColor(ofColor(120, 120, 120, 40));
    generalS = panel->addGroup("GENERAL SETTINGS ====");
    generalS->getHeader()->setBackgroundColor(gC);
    generalS->setBackgroundColor(ofColor(0, 120, 200));
    generalS->add<ofxGuiButton>(importSettings.set("Import Settings", false));
    generalS->add<ofxGuiButton>(exportSettings.set("Export Settings", false));
    generalS->add(tglAbout.set("About", false));
    generalS->add(tglFull.set("Toggle Fullscreen", false));
    generalS->add(useMouse.set("Use Mouse", false));
    generalS->add(goLive.set("GO LIVE!", false));
    oscSettings = generalS->addGroup("Osc Options");
    oscSettings->add<ofxGuiIntInputField>(portInInput.set("Receiver Port:", 6568));
    sceneS = panel->addGroup("SCENE SETTINGS ========");
    sceneS->getHeader()->setBackgroundColor(sC);
    sceneS->setBackgroundColor(ofColor(220, 135, 0, 80));
    sceneS->add(centerTrans.set("Translation Fix",ofVec2f(ofGetWidth()/2, ofGetHeight()*0.667), ofVec2f(0, 0), ofVec2f(ofGetWidth()*2,ofGetHeight()*2)));
    bgColor = sceneS->addGroup("Background Color");
    bgColor->add(bgColorR.set(">> RED", 220, 0, 255));
    bgColor->add(bgColorG.set(">> GREEN", 220, 0, 255));
    bgColor->add(bgColorB.set(">> BLUE", 220, 0, 255));
    bgColor->minimize();
    partSys = sceneS->addGroup("Particle Options");
    partSys->getHeader()->setBackgroundColor(ofColor(194, 0, 203, 255));
    partSys->setBackgroundColor(ofColor(255, 40, 255, 255));
    //partSys->add<ofxGuiIntInputField>(pCountW.set("Particle Count Width", w));
    //partSys->add<ofxGuiIntInputField>(pCountH.set("Particle Count Height", h));
    partColor = partSys->addGroup("Color");
    partColor->add(particleR.set(">> RED", 0.2, 0.0, 1.0));
    partColor->add(particleG.set(">> GREEN", 0.2, 0.0, 1.0));
    partColor->add(particleB.set(">> BLUE", 0.2, 0.0, 1.0));
    partColor->add(particleA.set(">> ALPHA", 0.2, 0.0, 1.0));
    partColor->minimize();
    partSys->add(switchIdThresh.set("Switch S1/S2", ofGetHeight()*0.667, 0, ofGetHeight()));
    partSys->add(showThreshLine.set("Show Switch Line", false)); 
    stateOne = partSys->addGroup("State 1 >> Rectangle");
    stateOne->add(maximumSpeed.set("Maximum Speed", 1.2, 0.1, 5.0));
    stateOne->minimize();
    stateTwo = partSys->addGroup("State 2 >> Circle");
    stateTwo->add(circleRadius.set("Radius", 300, 10, ofGetWidth()/2));
    stateTwo->add(center.set("Center",ofVec2f(ofGetWidth()/2, ofGetHeight()/2), ofVec2f(-ofGetWidth()/2,-ofGetHeight()/2), ofVec2f(ofGetWidth()*1.5,ofGetHeight()*1.5)));
    stateTwo->add(centerStiffness.set("Stiffness", 0.1, 0.01, 1));
    stateTwo->add(maximumSpeedCircle.set("Maximum Speed", 20, 1, 50));
    stateTwo->minimize();
    partSys->minimize();
    dlines = sceneS->addGroup("Line Options");
    dlines->getHeader()->setBackgroundColor(ofColor(0, 220, 0, 255));
    dlines->setBackgroundColor(ofColor(0, 180, 0, 255));
    dlines->add(bLH.set("Left Hand", true));
    dlines->add(bRH.set("Right Hand", true));
    dlines->add(bH.set("Hips", true));
    lLineColor = dlines->addGroup("Left Hand Color");
    lLineColor->add(lColorR.set(">> RED", 255, 0, 255));
    lLineColor->add(lColorG.set(">> GREEN", 0, 0, 255));
    lLineColor->add(lColorB.set(">> BLUE", 0, 0, 255));
    lLineColor->add(lColorA.set(">> ALPHA", 40, 0, 255));
    lLineColor->minimize();
    rLineColor = dlines->addGroup("Right Hand Color");
    rLineColor->add(rColorR.set(">> RED", 0, 0, 255));
    rLineColor->add(rColorG.set(">> GREEN", 255, 0, 255));
    rLineColor->add(rColorB.set(">> BLUE", 0, 0, 255));
    rLineColor->add(rColorA.set(">> ALPHA", 40, 0, 255));
    rLineColor->minimize();
    hLineColor = dlines->addGroup("Hips Color");
    hLineColor->add(hColorR.set(">> RED", 255, 0, 255));
    hLineColor->add(hColorG.set(">> GREEN", 255, 0, 255));
    hLineColor->add(hColorB.set(">> BLUE", 0, 0, 255));
    hLineColor->add(hColorA.set(">> ALPHA", 40, 0, 255));
    hLineColor->minimize();
    dlines->add(lineLength.set("Line Length", 1500, 20, 10000));
    dlines->add(N.set("N", 30, 1, 200));
    dlines->add(lineDist.set("Distance", 50, 1, 200));
    dlines->minimize();
    // OSC
    threadedOsc.setup(portIn);
}

//--------------------------------------------------------------
void ofApp::update(){
    ofBackground(bgColorR, bgColorG, bgColorB);
    width = ofGetWidth();
    height = ofGetHeight();
    mouseX = ofGetMouseX();
    mouseY = ofGetMouseY();
    receiveOsc();
    // Load settings if button is pressed
    if(importSettings)
    {
        importSettingsFile();
    }
    // Save settings if button is pressed
    if(exportSettings)
    {
        exportSettingsFile();
    }
    // Toggle fullscreen
    if(tglFull == true || bFull == true)
    {
        ofSetFullscreen(true);
    }
    else if(tglFull == true && bFull == false)
    {
        ofSetFullscreen(true);
        bFull = true;
    }
    else{
        ofSetFullscreen(false);
    }
    if(goLive == true && useMouse == false)
    {
        if(bPort == true)
        {
            //Reinitiate receiver with new port
            portIn = portInInput;
            threadedOsc.changePort(portIn);
            ofLog() << "listening for osc messages on port " << portIn;
            bPort = false;
        }

        if(centerTrans->y - posL.y < switchIdThresh  || centerTrans->y - posR.y < switchIdThresh){
            ofShader &shader = particles.getUpdateShader();
            shader.begin();
            shader.setUniform1f("maxSpeed", maximumSpeed);
            shader.setUniform1f("width", width);
            shader.setUniform1f("height", height);
            shader.end();

            particles.update();
        }
        else{
            ofShader &shader = particles.getUpdateShader(CIRCLE);
            shader.begin();
            shader.setUniform2f("center", center->x, center->y);
            shader.setUniform1f("radius", circleRadius);
            shader.setUniform1f("centerStiffness", centerStiffness);
            shader.setUniform1f("maxSpeed", maxSpeedCircle);
            shader.end();

            particles.update(CIRCLE);
        }
        // LEFT HAND DRAWING
        if(bLH)
        {
            if(int(linesL.size()) > lineLength * 10 || int(drawnPointsL.size()) > lineLength * 10)
            {
                linesL.clear();
                drawnPointsL.clear();
            }
            else if(int(linesL.size()) > lineLength)
            {
                linesL.erase(linesL.begin(), linesL.begin() + N);
            }
            for(auto point: drawnPointsL){
                positionL.set(centerTrans->x + posL.x , centerTrans->y - posL.y);
                float dist = (positionL - point).length();
                if(dist < lineDist){
                    Line lineTemp;
                    lineTemp.a = positionL;
                    lineTemp.b = point;
                    linesL.push_back(lineTemp);
                }
            }
            drawnPointsL.push_back(ofPoint(positionL.x, positionL.y));
        }
        if(bRH)
        {
            // RIGHT HAND DRAWING
            if(int(linesR.size()) > lineLength * 10 || int(drawnPointsR.size()) > lineLength * 10)
            {
                linesR.clear();
                drawnPointsR.clear();
            }
            else if(int(linesR.size()) > lineLength)
            {
                linesR.erase(linesR.begin(), linesR.begin() + N);
            }
            for(auto point: drawnPointsR){
                positionR.set(centerTrans->x + posR.x, centerTrans->y - posR.y);
                float dist = (positionR - point).length();
                if(dist < lineDist){
                    Line lineTemp;
                    lineTemp.a = positionR;
                    lineTemp.b = point;
                    linesR.push_back(lineTemp);
                }
            }
            drawnPointsR.push_back(ofPoint(positionR.x, positionR.y));
        }
        if(bH)
        {
            // RIGHT HAND DRAWING
            if(int(linesH.size()) > lineLength * 10 || int(drawnPointsH.size()) > lineLength * 10)
            {
                linesH.clear();
                drawnPointsH.clear();
            }
            else if(int(linesH.size()) > lineLength)
            {
                linesH.erase(linesH.begin(), linesH.begin() + N);
            }
            for(auto point: drawnPointsH){
                positionH.set(centerTrans->x + posH.x, centerTrans->y - posH.y);
                float dist = (positionH - point).length();
                if(dist < lineDist){
                    Line lineTemp;
                    lineTemp.a = positionH;
                    lineTemp.b = point;
                    linesH.push_back(lineTemp);
                }
            }
            drawnPointsH.push_back(ofPoint(positionH.x, positionH.y));
        }
    }
    else{
        bPort = true;
        if(useMouse == true)
        {
            if(mouseY < switchIdThresh){
                ofShader &shader = particles.getUpdateShader();
                shader.begin();
                shader.setUniform1f("maxSpeed", maximumSpeed);
                shader.setUniform1f("width", width);
                shader.setUniform1f("height", height);
                shader.end();
                particles.update();
            }
            else{
                ofShader &shader = particles.getUpdateShader(CIRCLE);
                shader.begin();
                shader.setUniform2f("center", center->x, center->y);
                shader.setUniform1f("radius", circleRadius);
                shader.setUniform1f("centerStiffness", centerStiffness);
                shader.setUniform1f("maxSpeed", maxSpeedCircle);
                shader.end();
                particles.update(CIRCLE);
            }
            //DRAW LINES
            if(int(lines.size()) > lineLength * 10 || int(drawnPoints.size()) > lineLength * 10)
            {
                lines.clear();
                drawnPoints.clear();
            }
            else if(int(lines.size()) > lineLength)
            {
                lines.erase(lines.begin(), lines.begin() + N);
            }
            for(auto point: drawnPoints){
                ofPoint mouse;
                mouse.set(mouseX, mouseY);
                float dist = (mouse - point).length();
                if(dist < lineDist){
                    Line lineTemp;
                    lineTemp.a = mouse;
                    lineTemp.b = point;
                    lines.push_back(lineTemp);
                }
            }
            drawnPoints.push_back(ofPoint(mouseX, mouseY));
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofEnableAlphaBlending();

    if(goLive == true || useMouse == false)
    {
        //Draw Particles
        ofShader &shader = particles.getDrawShader();
        shader.begin();
        shader.setUniformMatrix4f("projection", projection);
        shader.setUniformMatrix4f("modelview", modelView);
        shader.setUniform4f("color", particleR, particleG, particleB, particleA);
        shader.end();

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_PROGRAM_POINT_SIZE);
        particles.draw();
        glDisable(GL_PROGRAM_POINT_SIZE);
        glDisable(GL_BLEND);


        // Draw Lines from Left Hand position
        if(bLH)
        {
            ofSetColor(lColorR, lColorG, lColorB, lColorA);
            for(auto lineL: linesL){
                ofDrawLine(lineL.a, lineL.b);
            }
        }
        if(bRH)
        {
            // Draw Lines from Right Hand position
            ofSetColor(rColorR, rColorG, rColorB, rColorA);
            for(auto lineR: linesR){
                ofDrawLine(lineR.a, lineR.b);
            }
        }
        if(bH)
        {
            // Draw Lines from Right Hand position
            ofSetColor(hColorR, hColorG, hColorB, hColorA);
            for(auto lineH: linesH){
                ofDrawLine(lineH.a, lineH.b);
            }
        }
    }
    else{

        if(useMouse == true)
        {
            //ofEnableAlphaBlending();
            //Draw Particles
            ofShader &shader = particles.getDrawShader();
            shader.begin();
            shader.setUniformMatrix4f("projection", projection);
            shader.setUniformMatrix4f("modelview", modelView);
            shader.setUniform4f("color", particleR, particleG, particleB, particleA);
            shader.end();

            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glEnable(GL_PROGRAM_POINT_SIZE);
            particles.draw();
            glDisable(GL_PROGRAM_POINT_SIZE);
            glDisable(GL_BLEND);

            // Draw Lines
            ofSetColor(lColorR, lColorG, lColorB, lColorA);
            for(auto line: lines){
                ofDrawLine(line.a, line.b);
            }
        }

    }
    if(showThreshLine == true)
    {
        ofSetColor(180);
        ofDrawRectangle(0, int(switchIdThresh), ofGetWidth(), 2);
        ofSetColor(255);
    }


    if(bGui == false)
    {
        ofFill();
        ofSetColor(50, 50, 50, 255);
        ofDrawRectangle(0, height - 40, width, 40);
        ofSetColor(120, 200, 255);
        stringstream incomingTrans;
        incomingTrans << "Left Translation X: " + ofToString(posL.x) + " Left Translation Y: " + ofToString(posL.y) + " Right Translation X: " + ofToString(posR.x) + " Right Translation Y: " + ofToString(posR.y) + " Hips Translation X: " + ofToString(posH.x) + " Hips Translation Y: " + ofToString(posH.y)<< endl;
        incomingTrans << "Left Screen X: " + ofToString(positionL.x) + " Left Screen Y: " + ofToString(positionL.y) + " Right Screen X: " + ofToString(positionR.x) + " Right Screen Y: " + ofToString(positionR.y)<< endl;
        ofDrawBitmapString(incomingTrans.str().c_str(),  30, height - 20);
        ofSetColor(255);
    }


    // If "a" is pressed, about text will appear
    if (bAbout == true || tglAbout) {
        ofFill();
        ofSetColor(50, 50, 50, 255);
        ofDrawRectangle(width / 2 - width * 0.3, height / 2 - height * 0.1, width * 0.6, height * 0.3);
        ofSetColor(120, 200, 255);
        stringstream ss;
        ss << "Particulus >> Particle reactive liner" << endl;
        ss << endl;
        ss << "Version v0.1" << endl;
        ss << endl;
        ss << "This example uses received translation data from 'Chordata Mover'" << endl;
        ss << ">> Make sure 'Send Translation' is enabled in 'Chordata Mover' <<" << endl;
        ss << endl;
        ss << "Software developed by Jens Meisner (www.jensmeisner.net)" << endl;
        ss << endl;
        ss << "Licensed under GNU GENERAL PUBLIC LICENSE 3.0" << endl;
        ss << endl;
        ss << "contact@jensmeisner.net" << endl;
        ss << endl;
        ofDrawBitmapString(ss.str().c_str(), width / 2 - width * 0.3 + 40, height / 2 - height * 0.1 + 40);
        ofSetColor(255);
    }

}


//--------------------------------------------------------------
void ofApp::receiveOsc(){
    // Get position for left and right hand from thread
    posL = threadedOsc.getTransL();
    posR = threadedOsc.getTransR();
    posH = threadedOsc.getTransH();
}


// Load file dialog ------------------------------------------------
void ofApp::processOpenFileSelection(ofFileDialogResult openFileResult){
    // Get name and path of the file to be open
    ofLogVerbose("getName(): "  + openFileResult.getName());
    ofLogVerbose("getPath(): "  + openFileResult.getPath());
    //Set the file call
    ofFile file (openFileResult.getPath());
    // Proceed loading file, if file exists
    if (file.exists()){
        ofLogVerbose("The file exists - now checking the type via file extension");
        string fileExtension = ofToUpper(file.getExtension());
        // Check, if file has xml extension
        if(fileExtension == "XML")
        {
            // Save the file extension to use when we save out
            originalFileExtension = fileExtension;
        }
    }
}

//--------------------------------------------------------------
void ofApp::exportSettingsFile(){
    // Write all joint positions and cube references in converted millimeters into a XML file and save it
    ofFileDialogResult saveFileResult = ofSystemSaveDialog(ofGetTimestampString() + "." + ofToLower(originalFileExtension), "Save your SETTINGS file");
    // Check if the user opened a file
    if (saveFileResult.bSuccess){
        // We have a file, check it and process it
        //oscSettings->saveToFile(saveFileResult.getPath());
        bgColor->saveToFile(saveFileResult.getPath());
        partSys->saveToFile(saveFileResult.getPath());
        dlines->saveToFile(saveFileResult.getPath());
    }else {
        ofLogVerbose("User hit cancel");
    }
}

//--------------------------------------------------------------
void ofApp::importSettingsFile(){
    // Open the Open File Dialog
    ofFileDialogResult openFileResult= ofSystemLoadDialog("Select SETTINGS xml file");
    // Check if the user opened a file
    if (openFileResult.bSuccess){
        ofLogVerbose("User selected a file");
        // We have a file, check it and process it
        //oscSettings->loadFromFile(openFileResult.getPath());
        bgColor->loadFromFile(openFileResult.getPath());
        partSys->loadFromFile(openFileResult.getPath());
        dlines->loadFromFile(openFileResult.getPath());
    }else {
        ofLogVerbose("User hit cancel");
    }

}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key == '0') shaderId = 0;
    if (key == '1') shaderId = 1;
    if(key == 'f' || key == 'F')
    {
        bFull = !bFull;
    }
    if(key == 'm' || key == 'M')
    {
        bGui = !bGui;
        panel->setHidden(bGui);
    }
    if(key == 'a' || key == 'A')
    {
        bAbout = !bAbout;
    }
    if(key == 's') {
        panel->saveToFile("settings.xml");
    }
    if(key == 'l') {
        panel->loadFromFile("settings.xml");
    }
}

//--------------------------------------------------------------
void ofApp::resetParticleSys(){
    w = pCountW;
    h = pCountH;
    particles.init(w, h, ofPrimitiveMode::OF_PRIMITIVE_POINTS, 2);

    // Positions
    float* particlePosns = new float[w * h * 4];
    for (int y = 0; y < h; ++y)
    {
        for (int x = 0; x < w; ++x)
        {
            unsigned idx = y * w + x;
            particlePosns[idx * 4] =     ofRandom(ofGetWidth());
            particlePosns[idx * 4 + 1] = ofRandom(ofGetHeight());
            particlePosns[idx * 4 + 2] = 0.f;
            particlePosns[idx * 4 + 3] = 0.f;
        }
    }
    particles.loadDataTexture(FastParticleSystem::POSITION, particlePosns);
    delete[] particlePosns;
    // Velocities
    float* velocities = new float [w * h * 4];
    for (int y = 0; y < h; y++){
        for(int x = 0; x < w; x++){
            unsigned idx = y * w + x;
            velocities[idx * 4] =    ofRandom(-1.5, 1.5);
            velocities[idx * 4 +1] = ofRandom(-1.5, 1.5);
            velocities[idx * 4 +2] = 0;
            velocities[idx * 4 +3] = 0;
        }
    }
    particles.loadDataTexture(FastParticleSystem::VELOCITY, velocities);
    delete[] velocities;
    // Update shaders
    particles.addUpdateShader("shaders/updateParticlesRandom");
    particles.addUpdateShader("shaders/updateParticlesCircle", CIRCLE);
    // Draw shader
    particles.addDrawShader("shaders/drawParticles");
}

//--------------------------------------------------------------
void ofApp::exit(){
    threadedOsc.stop();
}
