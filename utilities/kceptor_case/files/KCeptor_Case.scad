thickness=6;
width=50;
height=43;
diameter=10;
$fn=30;

base=false;
lid=true;
logo=false;


//=========================================================================

module linear_array(count,distance)
{
    for(i=[0:1:count[0]-1])
    {
        for(j=[0:1:count[1]-1])
        {
            for(k=[0:1:count[2]-1])
            {
                translate([distance[0]*i,distance[1]*j,distance[2]*k])
                children();
            }
        }
    }
}


//=================================================================================
//Base

if(base)
{
    union()
    {
        difference()
        {
            difference()
            {
                union()
                {
                    hull()
                    {
                        translate([-width/2,-height/2,0])
                        cylinder(h=thickness+3,d=diameter+2);
                        translate([width/2,-height/2,0])
                        cylinder(h=thickness+3,d=diameter+2);
                        translate([width/2,height/2,0])
                        cylinder(h=thickness+3,d=diameter+2);
                        translate([-width/2,height/2,0])
                        cylinder(h=thickness+3,d=diameter+2);
                    }
                    difference()
                    {
                        scale([1,1,.3])
                        translate([0,0,45])
                        rotate([0,90,0])
                        cylinder(h=width+12,d=65,$fn=80,center=true);
                        scale([1,1,0.4])
                        translate([0,0,86])
                        rotate([0,90,0])
                        cylinder(h=width+12,d=110,$fn=80,center=true);
                    }
                    translate([0,0,thickness/2])
                    cube([width,height,thickness],center=true);
                    //Cable Clamp
                    difference()
                    {
                        hull()
                        {
                            translate([width/2+7,height/2-2,thickness/2])
                            cylinder(h=thickness,d=8,center=true);
                            translate([width/2+7,height/2-14,thickness/2])
                            cylinder(h=thickness,d=8,center=true);
                        }
                        union()
                        {
                            translate([width/2+7,height/2-8,thickness/2])
                            cube([3,11,thickness],center=true);
                            translate([width/2+7,height/2-16,thickness/2])
                            cube([2,5,thickness],center=true);
                        }
                    }
                    //Mirrored Cable Clamp
                    mirror([-1,0,0])
                    {
                        difference()
                        {
                            hull()
                            {
                                translate([width/2+7,height/2-2,thickness/2])
                                cylinder(h=thickness,d=8,center=true);
                                translate([width/2+7,height/2-14,thickness/2])
                                cylinder(h=thickness,d=8,center=true);
                            }
                            union()
                            {
                                translate([width/2+7,height/2-8,thickness/2])
                                cube([3,11,thickness],center=true);
                                translate([width/2+7,height/2-16,thickness/2])
                                cube([2,5,thickness],center=true);
                            }
                        }
                    }
                }
                union()
                {
                    //Holes and space holders for PCB
                    translate([width/2-11.5,height/2-8.5,1.5])
                    cube([13,17,3],center=true);
                    translate([-width/2+11.5,height/2-8.5,1.5])
                    cube([13,17,3],center=true);
                    translate([width/2-4,-height/2+5.5,0])
                    cylinder(h=thickness,d=3);
                    translate([-width/2+4,-height/2+5.5,0])
                    cylinder(h=thickness,d=3);
                    translate([-width/2+4,height/2-20,0])
                    cylinder(h=thickness,d=3);
                    difference()
                    {
                        hull()
                        {
                            translate([-width/2+0.5,-height/2+0.5,0])
                            cylinder(h=thickness/2,d=diameter);
                            translate([width/2-0.5,-height/2+0.5,0])
                            cylinder(h=thickness/2,d=diameter);
                            translate([width/2-0.5,height/2-0.5,0])
                            cylinder(h=thickness/2,d=diameter);
                            translate([-width/2+0.5,height/2-0.5,0])
                            cylinder(h=thickness/2,d=diameter);
                        }
                        hull()
                        {
                            translate([-width/2+3,-height/2+3.5,0])
                            cylinder(h=thickness/2,d=diameter);
                            translate([width/2-3,-height/2+3.5,0])
                            cylinder(h=thickness/2,d=diameter);
                            translate([width/2-3,height/2-3.5,0])
                            cylinder(h=thickness/2,d=diameter);
                            translate([-width/2+3,height/2-3.5,0])
                            cylinder(h=thickness/2,d=diameter);
                        }
                    }
                    hull()
                    {
                        translate([width/2+3.25,0,9])
                        cube([2.5,8,5],center=true);
                        translate([width/2+3.25,0,-4])
                        cube([2.5,height/2+3,5],center=true);
                    }
                    hull()
                    {
                        translate([-width/2-3.25,0,9])
                        cube([2.5,8,5],center=true);
                        translate([-width/2-3.25,0,-4])
                        cube([2.5,height/2+3,5],center=true);
                    }
                }
            }
            union()
            {
                // Patch space
                translate([0,0,13])
                cube([width,height-7,thickness],center=true);
                // Stripe holes
                translate([0,-height/2-12,0])
                rotate([-30,0,0])
                cube([50,5,80],center=true);
                translate([0,height/2+12,0])
                rotate([30,0,0])
                cube([50,5,80],center=true);
                rotate([0,90,0])
                translate([-4,0,30])
                cylinder(h=12,d=3,center=true);
                rotate([0,90,0])
                translate([-4,0,-30])
                cylinder(h=12,d=3,center=true);
            }
        }
        translate([0,-height/2-8.5,13.6])
        rotate([0,90,0])
        cylinder(h=height+diameter*1.9,d=7,center=true);
        translate([0,height/2+8.5,13.6])
        rotate([0,90,0])
        cylinder(h=height+diameter*1.9,d=7,center=true);
    }
}


//============================================================
//Lid

if(lid)
{
    rotate([0,180,0])
    translate([0,0,-8])
    union()
    {
        difference()
        {
            union()
            {
                hull()
                {
                    translate([width/2+3.2,0,3])
                    cube([2,7,5],center=true);
                    translate([width/2+3.2,0,9])
                    cube([2,height/2-2,5],center=true);
                }
                hull()
                {
                    translate([-width/2-3.2,0,3])
                    cube([2,7,5],center=true);
                    translate([-width/2-3.2,0,9])
                    cube([2,height/2-2,5],center=true);
                }
                difference()
                {
                    difference()
                    {
                        minkowski()
                        {
                            translate([0,0,10])
                            cube([width-1.5,height-1.9,22],center=true);
                            sphere(d=diameter);
                        }
                        minkowski()
                        {
                            translate([0,0,10])
                            cube([width-1.75,height-1.6,20],center=true);
                            sphere(d=diameter-4);
                        }
                    }
                    cube([60,height+20,12],center=true);
                    translate([0,0,26])
                    cube([48,38,3],center=true);
                }
                translate([0,0,25])
                rotate([0,0,30])
                cylinder(h=2,d=30,$fn=40,center=true);
                //
                translate([0,0,24.5])
                scale([1,1,.8])
                rotate([90,0,90])
                cylinder(h=48,d=4,$fn=6,center=true);
                //
                translate([0,5,24.5])
                scale([1,1,.8])
                rotate([90,0,90])
                cylinder(h=48,d=4,$fn=6,center=true);
                //
                translate([0,10,24.5])
                scale([1,1,.8])
                rotate([90,0,90])
                cylinder(h=48,d=4,$fn=6,center=true);
                //
                translate([0,15,24.5])
                scale([1,1,.8])
                rotate([90,0,90])
                cylinder(h=48,d=4,$fn=6,center=true);
                //
                translate([0,20,24.5])
                scale([1,1,.8])
                rotate([90,0,90])
                cylinder(h=48,d=4,$fn=6,center=true);
                //
                translate([0,-5,24.5])
                scale([1,1,.8])
                rotate([90,0,90])
                cylinder(h=48,d=4,$fn=6,center=true);
                //
                translate([0,-10,24.5])
                scale([1,1,.8])
                rotate([90,0,90])
                cylinder(h=48,d=4,$fn=6,center=true);
                //
                translate([0,-15,24.5])
                scale([1,1,.8])
                rotate([90,0,90])
                cylinder(h=48,d=4,$fn=6,center=true);
                //
                translate([0,-20,24.5])
                scale([1,1,.8])
                rotate([90,0,90])
                cylinder(h=48,d=4,$fn=6,center=true);
                translate([12,-21,24])
                cylinder(h=4,d=5,center=true);
            }
            union()
            {
                rotate([0,90,0])
                translate([-4,0,30])
                cylinder(h=12,d=3,center=true);
                rotate([0,90,0])
                translate([-4,0,-30])
                cylinder(h=12,d=3,center=true);
                translate([0,height/2-8.5,15])
                cube([80,16,13],center=true);
                translate([0,0,26])
                cylinder(h=4,d=26,$fn=40,center=true);
                translate([12,-21,24.5])
                cylinder(h=18,d=4,center=true);
            }
        }
        translate([0,0,24.5])
        cylinder(h=3,d=20,$fn=40,center=true);
    }
}


//================================================================
//Logo 

if(logo)
{
    translate([0,0,-18])
    rotate([0,180,-90])
    union()
    {
        
        difference() 
        {

            translate([0,0,0])
            cylinder(h=1.5,d=25,$fn=40,center=true);
            translate([0,0,0])
            cylinder(h=1.5,d=21,$fn=40,center=true);
        }
        difference()
        {
            hull()
            {
            translate([0,0,1])
            cylinder(h=1,d=27,$fn=40,center=true);
            translate([0,0,2])
            cylinder(h=2,d=20,$fn=40,center=true);
            }
            translate([0,0,0])
            linear_extrude(8)
            
            
//            text("lh",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("lfa",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("la",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("rh",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("rfa",size=10,font="Arista:style=Regular",halign="center",valign="center");
//            text("ra",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("h",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("c",size=11,font="Arista:style=Regular",halign="center",valign="center");  
//            text("b",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("lt",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("ll",size=11,font="Arista:style=Regular",halign="center",valign="center");
//       text("lf",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("rt",size=11,font="Arista:style=Regular",halign="center",valign="center");
//            text("rl",size=11,font="Arista:style=Regular",halign="center",valign="center");
            text("rf",size=11,font="Arista:style=Regular",halign="center",valign="center");
    }   
//        translate([0,0,1.75])
//        cube([1,10,2.5],center=true);
    }
}