
$fn=80;

rBall = 20;
hHole = 15;
rHole = 3;

difference()
{
    color("DarkSlateGray")
    union()
    {
        sphere(rBall);
        difference()
        {
            cylinder(r = rBall, h = rBall * 2 , center=true);
            translate([0, 0, rBall])
            cube(rBall * 2, center=true);
        }
    }
    union()
    {
        translate([0,0,rBall-hHole])
        cylinder(h=hHole, r=rHole+0.05);
        rotate([0, 90, 0])
        translate([0,0,rBall-hHole])
        cylinder(h=hHole, r=rHole);
        rotate([90, 0, 0])
        translate([0,0,rBall-hHole])
        cylinder(h=hHole, r=rHole);
    } 
}


