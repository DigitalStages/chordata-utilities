# Chordata Utilities

The repository contains utilities for the Chordata project, including designs for hardware and software supporting equipment.

-----------

* **README.md** >> Photo assembly guide

* **shutdown_button** >> Python script and service script for  Raspberry Pi Shutdown Button connected to GPIO Pin 20

* **relativity_cube >>**

  * **img** >> Renders and photo of Design
  * **scad** >> Source file in .scad format
  * **stl** >> Exported 3D meshes in .stl format

  | Render | Photo |
  | ----------- | ----------- |
  | ![](./relativity_cube/images/relativity_cube_render.jpg)| ![](./relativity_cube/images/relativity_cube_final.jpg) |

* **kceptor_case >>**

  * **gcode** >> Example files of working gcode printable files >>
  * **img** >> Renders and photo of Design
  * **scad** >> Source file in .scad format
  * **stl** >> Exported 3D meshes in .stl format

  | Render | Photo |
  | ----------- | ----------- |
  | ![](./kceptor_case/images/kceptor_case.png)| ![](./kceptor_case/images/kceptor_case_Photo.png) |



* **rpi_hub_case >>**

  * **gcode** >> Example files of working gcode printable files
  * **img** >> Renders and photo of Design
  * **scad** >> Source file in .scad format
  * **stl** >> Exported 3D meshes in .stl format

  | Render | Photo |
  | ----------- | ----------- |
  | ![](./rpi_hub_case/images/rpi_hub_case.png)| ![](./rpi_hub_case/images/rpi_hub_case_Photo.png) |


----

# Instructions

## Relativity Cube

You need this cube in order you create a referencing ratio, that will create a close to reality representation of the performers body. Please follow the guide lines in the description of Software "Chordata Sizer"

#### What you need:
* 3D Printer
* Cutting tool
* Saw (if the aluminum rods are longer)


| 1. Measure 48 cm for each rod, and cut it 12 times, resulting in 12 rods aka edges of a cube |
| ----------- |
|2. Print 8 pieces of the corner pieces|
|![](./relativity_cube/images/relativity_cube_parts.jpg)|
|3. Stick all parts together|
|![](./relativity_cube/images/relativity_cube_assembled.jpg)|

----

## KCeptor Sensor Case (Beta Version)


#### What you need:
* Foam 10 mm thick
* Ruler
* Pen
* Universal glue
* Cutter
* Screw driver
* Screws 3mm



    1. Measure 35 mm stripes and separate it each 50 mm

    2. Use the glue to attach a foam patch on each bottom...

    3. ...shown here

    4. Use screw driver to mount sensor board on 3 points...

    5. ...like shown here

    6. Use glue to attach the label buttons onto the lid (see also 8.)  

    7. Lead LED light pipe through small hole. Press it later down, after lid and base are put together

    8. Keep in mind, that chest "c" and "h" are the 2 exception, thus need to get attached 180 degrees upside down.


  ![](./kceptor_case/images/kceptor_case_Assembly.png)

-----


## Case for Raspberry Pi / K-Ceptor Hub (Raspberry Pi 2, 3, 4)

#### What you need:
* Foam 10 mm thick
* Ruler
* Pen
* Universal glue
* Cutter
* Soldering set
* Screw driver
* Screws 3mm


    1. Cut a foam patch 60 x 70 mm and glue it on the bottom of the base

    2. Screw lid on top part

    3. Solder micro USB plug >> + pol to switch, from other pol of switch to + pol of supply cable. Solder the - pol of the USB plug to the - pol of the power supply.

    4. Solder from switch to LED + pol, from LED - pol to 580 Ohm , from 580 Ohm to - pol of power supply cable.

    5. Connect wires from Hub to Raspberry Pi GPIO pins as described in Chordata video tutorial.

    6. Cut another cable you use in 5. by cutting it in half, and solder the ends to the small push button, that gets connected to Ground and GPIO pin 20.

    7. Use a hot glue gun to attach LED and push button, anything that needs some hold (or use the .scad file to advance the design)

    8. Connect 2 shorter and 4 longer cables of the kit (4 outside, 2 in middle position) to the hub, and double lock the cable 6 cover + 6 screws.

> #### Check on the switch and LED, boot the Raspberry Pi, test the configuration and functionality of the hub, and test the shutdown button, before you hot glue and put all parts together!

  ![](./rpi_hub_case/images/rpi_hub_case_Assembly.png)

-----
